// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//  RPC implementation
//
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <chrono>
#include <upcxx/upcxx.hpp>

#include "types.hpp"
#include "utils.hpp"
#include "OrderedStream.hpp"
#include "Comm.hpp"

#include "gup.hpp"
#include "gup_utils.hpp"

using namespace std;
using namespace upcxx;

extern int nentries;

// extern int k_factor;

global_ptr<uint64> upd_table;

// Target histogram
//uint64 *targetHisto;


// Ignore aggregation length n_agg for unaggregated version
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    // Atomic update version
    alg_name = string("AGG-UXX");
    intrank_t myrank = comm->myRank();
    intrank_t nranks = comm->nRanks();

    int64 i0, i1, locTabSize;
    int x, y;
    const int kfac = 16;
    // uint64 agg = 512;
    const uint64 expected_messages = Vlen / nranks;

    const double fudge_factor = 1.3;
    const int k_expect = floor(fudge_factor * ((double) (kfac * expected_messages)));

    const int buff_size = k_expect * nranks;

    // Table containing all the updates of a batch from the source process
    uint64* a = new uint64[buff_size];

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks];

    // Counter to know how many messages had been sent to a target (for the version using aggregation factor)
    int* factor = new int[nranks];

    uint64 num_bis = 0; // Same role than factor but fo the reveiver
    uint64 num; //counter of the number of messages for the receiver

    Block(myrank, nranks, tabsize, i0, i1, locTabSize);


    // Allocate a distributed table in the shared segment
    // Each rank holds a portion of the table
    uint64 *table = new uint64[locTabSize];
    assert(table);
    upd_table = new_array<uint64>(buff_size);

    assert(!upd_table.is_null());

    global_ptr<uint64> *udirectory = new global_ptr<uint64>[nranks];
    assert(udirectory);

    // To build this directory, we use a UPC++ dist_obj
    // which is constructed collectively
    upcxx::dist_object<global_ptr<uint64>> upd_PTab(upd_table);


    // With the dist_obj in hand, each rank can index the object
    // via the fetch() operation and thus fill the directory
    // Fetch() is like an RPC, and we must wait for it to complete
    // Another way to build the directory is to use all_gather(),
    // but UPC++ doesn't support the operation natively

    for (int i=0; i<nranks; i++){
        udirectory[i] = upd_PTab.fetch(i).wait();
    }
    // Need to ensure quiescence of the collective construction
    upcxx::barrier();

    // We next initialize the main table
    // We use load store semantics;  we can downcast to a raw C++ pointer,
    // a global pointer with affinity to this rank

    for (int i = i0, j=0; i <= i1; i++, j++)   // converts to local view index
	table[j] = i;

    // targetHisto = new uint64[nranks];
    // assert(targetHisto);
    // for (intrank_t i = 0; i < nranks; i++)
    //     targetHisto[i] = 0;

    // An atomic domain is used to set up the fast updating put (xor)
    // upcxx::atomic_domain<uint64_t> ad_ui64({upcxx::atomic_op::bit_xor});

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;
    uint64  ran = startr(start);
    // ostringstream sl;

    const int64 tabszm1 = tabsize-1;
    const int64 iTabSz = log2(locTabSize);
    const int64 ltabszm1 = locTabSize-1;
    const int   freqm1 = freq-1;

    // We add an extra iteration if nreps is odd; this enables us
    // to use reversibilty property of the updates to perform rapid verification
    int nr = nreps + (nreps%2);

    // beginning of the algorithm
    for (int r=0; r< nr; r++){

        // Restart the random number generator
        ran = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        for (int k=0; k<nbatch; k++) {

            //reinitialize the counter table
            for(x = 0; x < nranks; x++){
               counter[x] = 1;
               factor[x] = 0;
            }

            int too_many = 0; int* ttarg = new int[nranks];
            for (int k = 0; k < nranks; k++) ttarg[k] = 0;

            const int my_koff = myrank * k_expect;
            // Synchronized using promises
            // A promise object counts the number of outstanding ops
            promise<> batch_done;
            for (int j=0; j<Vlen; j++) {

                ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
                const int64 indxG = ran & tabszm1;         // Truncate to global table size
                const uint64 targ = indxG >> iTabSz;       // The target
                // const uint64 offs = indxG & ltabszm1;      // The offset

                // bool too_many = false;

                // targetHisto[targ]++;

                if(targ != myrank){
                    if(1 + counter[targ] >= k_expect){
                        if (!ttarg[targ])
                            fprintf(stderr,"%sToo many messages from %d to %lld (counter=%d, k_expect=%d)%s\n", constants::YELLOW, myrank, (long long int) targ, counter[targ],k_expect, constants::BACK);
                        ttarg[targ]++; too_many++;
                    }
                    else{
                        a[targ * k_expect + counter[targ]] = ran;
                        counter[targ]++;
                    }

                    if(counter[targ] == (1 + factor[targ]) * n_agg){
                        const int n_buffered = factor[targ] * n_agg;
                        assert(n_buffered < k_expect);
                        int64 offs_a  = targ * k_expect + n_buffered;
                        assert(offs_a < buff_size);
                        uint64 *pa = a + offs_a;
                        *pa = n_agg - 1;
                        // a[targ * (kfac * expected_messages) + factor[targ] * agg] = agg - 1;
                        // factor[targ]++;

                        // rput(a + targ * kfac * expected_messages + (factor[targ] - 1) * agg, udirectory[targ] + myrank * kfac * expected_messages + (factor[targ] - 1) * agg, agg, upcxx::operation_cx::as_promise(batch_done));

                        rput(pa, udirectory[targ] + my_koff + n_buffered, n_agg, upcxx::operation_cx::as_promise(batch_done));

                        factor[targ]++;
                        counter[targ]++; //To put the counter of the next buffer
                    }
                }
                else{ // if we are the target, we directly update our table 
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= ran;
                    counter[targ]++;
                }
                // assert(!too_many);
            }
            // Return a future when all the promises have been fulfilled
            // Counter reaches zero
            future<> fut = batch_done.finalize();
            fut.wait();
            upcxx::barrier();

            // int g_too_many = comm->gsum_int(too_many,true);
            // if ((g_too_many > 0) && (!myrank))
            //     cerr << constants::RED << "** A total of " << g_too_many << " buffering ops resulted in overflow" << constants::BACK<< endl;

            // // Everyone will execute this code or none at all
            // if (g_too_many > 0) {
            //     comm->Barrier();
            //     char buffer[128];
            //     sprintf(buffer,"%sToo many messages!%s", constants::YELLOW, constants::BACK);

            //     FATAL_ERR_NO_RTN_EXIT(buffer);
            // }

            promise<> remainder_done;

            //sending the remaining updates
            for(int t = 0; t < nranks; t++){
                x = (myrank + t) % nranks;

                if(x != myrank){
                    const int n_buffered_remain = factor[x] * n_agg;
                    assert(n_buffered_remain < k_expect);
                    int remain = counter[x] - n_buffered_remain;
                    assert(remain < n_agg);
                    assert(remain>0);
                    
                    // a[x * (kfac * expected_messages) + factor[x] * agg] = remain - 1;

                    uint64 ofs = x * k_expect + n_buffered_remain;
                    uint64 *pa_remain = a + ofs; *pa_remain = remain - 1;

                    // factor[x]++;
                    // rput(a + x * kfac * expected_messages +
                    //      (factor[x] - 1) * agg, udirectory[x] + myrank *
                    //       kfac * expected_messages + (factor[x] - 1) * agg, remain,
                    //       upcxx::operation_cx::as_promise(remainder_done));
                
                    rput(pa_remain, udirectory[x] + my_koff + n_buffered_remain, remain,upcxx::operation_cx::as_promise(remainder_done));
                }
            }

            future<> fut_r = remainder_done.finalize();
            fut_r.wait();
            upcxx::barrier();

            uint64 *upd_pt = upd_table.local();

            // Local updates
            for(int x = 0, xi = 0; x < nranks; x++, xi+=k_expect){
                num_bis = 0; // Same role than factor (to know the index of the buffer received)
                if(x != myrank){
                    num = upd_pt[xi]; //counter of the number of messages
                    assert(num);
                    bool last_one = num != n_agg - 1;
                    while(true){
                        const int n0 = x * k_expect + num_bis * n_agg + 1;
                        for(y = n0; y < n0 + num; y++){
                            uint64 rans = upd_pt[y];
                            const int64 indxG = rans & tabszm1;
                            const uint64 offs = indxG & ltabszm1; // Determine offset
                            table[offs] ^= rans;
                        }
                        if (last_one)
                            break;

                        num_bis++;
                        num = upd_pt[n0+n_agg-1];
                        last_one = (num != n_agg-1);
                    }
 
                    // while(num == agg - 1){
                    //     for(y = x * kfac * expected_messages + num_bis * agg + 1; y < x * kfac * expected_messages + num_bis * agg + 1 + num; y++){
                    //         uint64 rans = upd_pt[y];
                    //         const int64 indxG = rans & tabszm1;
                    //         const uint64 offs = indxG & ltabszm1; // Determine offset
                    //         table[offs] ^= rans;
                    //     }
                    //     num_bis++;
                    //     num = upd_pt[x * kfac * expected_messages + num_bis * agg]; 
                    // }

                    // // Update for the last received buffer
                    // for(y = x * kfac * expected_messages + num_bis * agg + 1; y < x * kfac * expected_messages + num_bis * agg + 1 + num; y++){
                    //     uint64 rans = upd_pt[y];
                    //     const int64 indxG = rans & tabszm1;
                    //     const uint64 offs = indxG & ltabszm1; // Determine offset
                    //     table[offs] ^= rans;
                    // }


                }
            }
            
            // Ensures quiescence
            upcxx::barrier();
        }

        // Barrier ensures global quiescence before verification begins
        // Otherwise we could get verification failures with sufficient load
        // imbalance, and/or contamination of the timed region with concurrent
        // verification code
        comm->Barrier();

        // End update timed section
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg,
                          freq, dummy_stats, doVerification, dummyMode);
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();

    // Verification
    int64 totErrors = -1;
    if (doVerification){
        // pt is a C++ pointer to the local portion of the shared heap
        totErrors = Verify(locTabSize,nupdate, table, i0, i1, tabsize, errByRank);
        if (!myrank){
            if (!totErrors)
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl;
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }
#ifdef SHOW_COUNTS
    sl << "+@, ";
    for (intrank_t k = 0; k< nranks; k++)
        sl <<  targetHisto[k]  << ", ";
    sl << endl;
#endif

    //sordst.OutputStream(sl);

    // ad_ui64.destroy();
    //delete [] ldirectory;
    delete [] udirectory;
    //delete [] targetHisto;
    delete [] table;
    delete_array(upd_table);
    return totErrors;
}
