// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//  RPC implementation
//
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <upcxx/upcxx.hpp>

#include "types.hpp"
#include "utils.hpp"
#include "OrderedStream.hpp"
#include "Comm.hpp"

#include "gup.hpp"
#include "gup_utils.hpp"

using namespace std;
using namespace upcxx;

extern int nentries;

// Main table
global_ptr<uint64> table;

// Target histogram
uint64 *targetHisto;

void init_AlgName(){
    // Atomic update version
    alg_name = string("UNAGG-AUXX");
}


// Ignore aggregation length n_agg for unaggregated version
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    // Atomic update version
    alg_name = string("UNAGG-AUXX");
    intrank_t myrank = comm->myRank();
    intrank_t nranks = comm->nRanks();

    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);


    // Allocate a distributed table in the shared segment
    // Each rank holds a portion of the table
    table = new_array<uint64>(locTabSize);
    assert(!table.is_null());


    // In order to update the global table at remote locations, a rank
    // needs to know the base address of the table allocated by other ranks
    // It uses this base address - a global pointer - to create global
    // references to remote table entries for subsequent updating

    // Initially, however, a rank knows only the address of the table that
    // it allocated.  So it builds a directory holding the global
    // pointers to the tables allocated by all the other ranks
    global_ptr<uint64> *ldirectory = new global_ptr<uint64>[nranks];
    assert(ldirectory);

    // To build this directory, we use a UPC++ dist_obj
    // which is constructed collectively
    upcxx::dist_object<global_ptr<uint64>> PTab(table);


    // With the dist_obj in hand, each rank can index the object
    // via the fetch() operation and thus fill the directory
    // Fetch() is like an RPC, and we must wait for it to complete
    // Another way to build the directory is to use all_gather(),
    // but UPC++ doesn't support the operation natively

    for (int i=0; i<nranks; i++)
        ldirectory[i] = PTab.fetch(i).wait();

    // Need to ensure quiescence of the collective construction
    upcxx::barrier();

    // We next initialize the main table
    // We use load store semantics;  we can downcast to a raw C++ pointer,
    // a global pointer with affinity to this rank

    assert(table.is_local());
    uint64 *pt = table.local();


    for (int i = i0, j=0; i <= i1; i++, j++)   // converts to local view index
	pt[j] = i;

    targetHisto = new uint64[nranks];
    assert(targetHisto);
    for (intrank_t i = 0; i < nranks; i++)
        targetHisto[i] = 0;

    // An atomic domain is used to set up the fast updating put (xor)
    upcxx::atomic_domain<uint64_t> ad_ui64({upcxx::atomic_op::bit_xor});

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;
    uint64  ran = startr(start);
    ostringstream sl;

    const int64 tabszm1 = tabsize-1;
    const int64 iTabSz = log2(locTabSize);
    const int64 ltabszm1 = locTabSize-1;
    const int   freqm1 = freq-1;

    // We add an extra iteration if nreps is odd; this enables us
    // to use reversibilty property of the updates to perform rapid verification
    int nr = nreps + (nreps%2);

    for (int r=0; r< nr; r++){

        // Restart the random number generator
        ran = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        for (int k=0; k<nbatch; k++) {

            // Synchronized using promises
            // A promise object counts the number of outstanding ops
            promise<> atos_done;
            for (int j=0; j<Vlen; j++) {

                ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
                const int64 indxG = ran & tabszm1;         // Truncate to global table size
                const uint64 targ = indxG >> iTabSz;       // The target
                const uint64 offs = indxG & ltabszm1;      // The offset


                targetHisto[targ]++;

                // update the table
                // We can't use C++ atomics, because the semantics
                // of a C++ atomic aren't compatible with
                // nicely with remote atomic updates
                // ldirectory[targ] points at the start of the
                // table at the target, which was previously initialized
                ad_ui64.bit_xor(ldirectory[targ]+offs, ran, memory_order_relaxed, upcxx::operation_cx::as_promise(atos_done));



            }
            // Return a future when all the promises have been fulfilled
            // Counter reaches zero
            future<> fut = atos_done.finalize();
            fut.wait();

            // Ensures quiescence
            upcxx::barrier();
        }

        // Barrier ensures global quiescence before verification begins
        // Otherwise we could get verification failures with sufficient load
        // imbalance, and/or contamination of the timed region with concurrent
        // verification code
        comm->Barrier();

        // End update timed section
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg,
                          freq, dummy_stats, doVerification, dummyMode);
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();

    // Verification
    int64 totErrors = -1;
    if (doVerification){
        // pt is a C++ pointer to the local portion of the shared heap
        totErrors = Verify(locTabSize,nupdate,pt, i0, i1, tabsize, errByRank);
        if (!myrank){
            if (!totErrors)
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl;
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }
#ifdef SHOW_COUNTS
    sl << "+@, ";
    for (intrank_t k = 0; k< nranks; k++)
        sl <<  targetHisto[k]  << ", ";
    sl << endl;
#endif

    sordst.OutputStream(sl);

    ad_ui64.destroy();
    delete [] ldirectory;
    delete [] targetHisto;
    delete_array(table);
    return totErrors;
}
