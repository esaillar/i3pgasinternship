// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//
// Aggregated rpc variant, January 2020, Scott B. Baden
//

#include <upcxx/upcxx.hpp>

#include "gup.hpp"
#include "utils.hpp"
#include "Comm.hpp"
#include "OrderedStream.hpp"

#include <assert.h>

using namespace std;
using namespace upcxx;

Comm *Comm::instance = 0;
Comm *comm = comm->getInstance();

// Set up ordered stream to organize output by rank
OrderedStream sordst;

// Used to report summary stats
double *wallTimes;
int nentries;

// Not used now, but will be useful with aggregation 
int *maxSlotsHW;
int64 *maxSlots;

int Mod = 0;

string alg_name; // algorithm name

void cmdLine(int argc, char *argv[], int& l_tabsize, int& m_update, int& nreps,
        int& Vlen, int& n_agg, int& kfactor,
        bool& printAff, bool& errorByRank, bool& doVerification,
        int& freq, int& mode);


int main(int argc, char *argv[])
{
   comm->Init(&argc, &argv);
   intrank_t myrank = comm->myRank();
   intrank_t nranks = comm->nRanks();

// Initialize the ordered stream service
// At the end of the program, close down the service with CloseStream()
  sordst.Init();

// Demonstration of how to use ordered streaming

// Declare an ordered stream
  ostringstream sl;

// Use it as you would ordinary stream output
//  sl << "Hello from Rank " << myrank << endl;
 
// Flush the output
  sordst.OutputStream(sl);

// You can continue to output to the stream in a separate "batch"
// The ordered stream service will only output one line if
// all ranks output the same text

//  sl << "There are " << nranks << " ranks" << endl;

// Flush the output for this batch
  sordst.OutputStream(sl);

// Default parameter values

// Log of the size of main table (suggested: half of global memory)
    int l_tabsize = 25;

// Multiplier for # updates to table (suggested: 4x number of table entries)
    int m_update = 4;

// Launch window size
// If we specify V=1, we have just 1 batch
    int Vlen     = 1024;

// Aggregation factor (n_agg > 0): we aggregate in buffers of size n_agg
// MPI variant will also handle n_agg = 0
    int n_agg = 0;       // There is no aggregation when n_agg=0

    // Errors will occur when there are data races

    int nreps = 3;       // # repetitions

//  Progress advancement frequency (must divide Vlen and be a power of 2)
    int freq     = 4;

    bool errByRank = false;
    bool doVerification = false;

// End setting up default parameter values

    //  kfactor is not used in native RPC version
    int kfactor = -1; 

    //  Not used currently, but could be
    bool printAff = false;

    //  Not used currently, but can be used to
    //  select programmer defined options
    int Mode = 0;

    cmdLine(argc, argv, l_tabsize, m_update, nreps, Vlen, n_agg, kfactor,
            printAff, errByRank, doVerification, freq, Mode);

    int ltabsize = l_tabsize;
    const int64 tabsize = ((int64) 1) << (int64) l_tabsize;
    const int64 nupdate = ((int64) m_update) * tabsize;
    if ( (nranks <= 0) || (!((nranks & (nranks-1)) == 0)) || (tabsize % nranks)){
        char Buff[132];
        sprintf(Buff," # ranks (%d) must be a power of 2 that evenly divides the global table size (%lld)\n",nranks,(long long) tabsize);

        FATAL_ERR(Buff);
    }

    if (nupdate % Vlen) FATAL_ERR("Vlen [" << Vlen << "] must evenly divide nupdate [" << nupdate << "]");
    
    // freq must be non-negative and a power of 2
    if ( (freq <= 0) || (!((freq & (freq-1)) == 0)) || ((Vlen > 1) && (Vlen % freq)))
        FATAL_ERR("Frequency [" << freq << "] must be a power of 2 that divides the batch length [" << Vlen << "]");


    // Print parameters for run
    if (!myrank){
        double slop = 8;
        printf("nThreads = %d\n", nranks);
        printf("Main table size = 2^%d = ", ltabsize);
        // Formatting of 64 bit types: https://en.cppreference.com/w/cpp/types/integer
        printf("%" PRId64 " words\n", tabsize);
        printf("Number of updates = %" PRId64 "\n", nupdate);
        if (n_agg > 0)
            printf("Aggregation factor = %d\n", n_agg);
        else if (n_agg == 0){
            printf("%sNo aggregation%s\n", constants::RED,constants::BACK); 
            if (Vlen == 1)
                printf("Number of batches = %d\n", 1);
            else
                printf("Number of batches = %" PRId64 "\n", nupdate/nranks/Vlen);
        }
        cout << "Progress frequency: " << freq << endl;
        if (nreps > 0){
            cout << "Performing " << nreps << " repetitions of each run\n";
        }
        if (Mode != 0)
            cout << "Mode: " << Mode << endl;
    }

    comm->report_uxx();


    // Initialize summary stats tables
    assert(wallTimes = new double[nreps]);
    assert(maxSlots = new int64[nreps]);
    assert(maxSlotsHW = new int[nreps]);

    comm->reportSegUsage("Before Calling update_table()");
    comm->Barrier();

    nentries = 0;
    // Always use source completion
    int64 totErrs = update_table(nreps, Vlen, n_agg, ltabsize, tabsize, nupdate, freq, errByRank,doVerification);
//    int64 totErrs = update_table_base(nreps, Vlen, n_agg, ltabsize, tabsize, nupdate, freq, errByRank,doVerification);

    comm->Barrier();

    if (!myrank){
        cout << endl;
        cout << "<---< Run ends   at ";
        timestamp(true);
    }

    if (nreps > 1)
        reportSummary(nreps, nupdate, Vlen, n_agg, false,doVerification,totErrs, Mode);

    comm->reportSegUsage("After Calling update_table()");
    comm->Barrier();

// Close down the ordered stream service
    sordst.CloseStream();
    comm->Barrier();

    comm->Finalize();
    delete [] wallTimes;
    delete [] maxSlots;
    delete [] maxSlotsHW;
 
    return(0);
}
