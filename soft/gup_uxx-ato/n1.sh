#!/bin/bash
#SBATCH -p fpgaq # partition (queue)
# #SBATCH -p rome16q # partition (queue)
#SBATCH -N 1 # number of nodes
#SBATCH -n 32  # number of cores
#SBATCH -t 0-10:00 # time (D-HH:MM)
#SBATCH --job-name=N1_GUP_UNAGG

#SBATCH -o %x-%j.out   # output and error file name
#SBATCH -e %x-%j.out   # output and error file name
                       #  (%j expands to jobID)
                       #  (%x expands to job name)
                       # "filename pattern" in https://slurm.schedmd.com/sbatch.html
# #SBATCH -o output/defq/slurm.%N.%j.out               # STDOUT
# #SBATCH -e output/defq/slurm.%N.%j.err            # STDERR
# #SBATCH -o %x-%j.out   # output and error file name

#SBATCH --mail-user=baden@ucsd.edu
#SBATCH --mail-type=begin
#SBATCH --exclusive

module load slurm/20.02.7
module load openmpi/gcc/64/4.0.5

ulimit -s 10240

export OMPI_MCA_pml="^ucx"
# export OMPI_MCA_btl_openib_if_include="mlx5_4:1"
export OMPI_MCA_btl=openib,self


# export OMPI_MCA_pml_ucx_verbose=100
echo " === Environment:"
printenv 


echo ""
echo "==========================================================="
echo ""
lscpu
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


export LAUNCH="upcxx-run -v -shared-heap 256M"
echo "Launching with: " ${LAUNCH}
export L_SIZE="27"
echo "L = " ${L_SIZE}
export GUP="./gup-unagg"
echo "GUP = " ${GUP}
echo ""
echo "==========================================================="
echo ""
echo "Run starts at $(date) on $(uname -n)"

$@



export OMPI_MCA_btl_openib_warn_no_device_params_found=1
export OMPI_MCA_btl_openib_if_include=mlx5_1
export OMPI_MCA_btl=^ucx,openib,tcp

# export OMPI_MCA_pml_ucx_verbose=100
echo " === Environment:"
printenv 


echo ""
echo "==========================================================="
echo ""
lscpu
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


echo "Run starts at $(date) on $(uname -n)"


upcxx-run -n 32 ./gup-unagg -l 25 -r 2 -c -v 262144
upcxx-run -n 32 ./gup-agg -l 25 -r 2 -c -v 262144 -a 512
# upcxx-run -n 64 ./gup-unagg -l 25 -r 2 -c -v 262144
# ${LAUNCH} -n $SLURM_NTASKS ./aggy -l ${L_SIZE} -v 1048576 -a 512 -c -r 1


echo "Run ends at $(date) on $(uname -n)"
