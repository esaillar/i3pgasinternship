#!/bin/bash
#SBATCH --qos=debug
# #SBATCH --qos=regular
# #SBATCH --qos=overrun
# #SBATCH --time-min=00:30:00
#SBATCH -A m2878
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=32
#SBATCH -C haswell
# #SBATCH --constraint=haswell
#SBATCH --time=00:30:00
#SBATCH --job-name=N8_GUP_UNAGG_UXX
#
# Documentation:
# https://slurm.schedmd.com/documentation.html
# Slurm environment variables
# http://www.glue.umd.edu/hpcc/help/slurmenv.html


#SBATCH -o %x-%j.out   # output and error file name
                       #  (%j expands to jobID)
                       #  (%x expands to job name)
                       # "filename pattern" in https://slurm.schedmd.com/sbatch.html
#SBATCH --mail-user=baden@ucsd.edu
#SBATCH --mail-type=begin

#module load perftools-base perftools-lite
module list 

echo " === Environment:"
printenv 


# cat /proc/cpuinfo

echo ""
echo "==========================================================="
echo ""
lscpu
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


export LAUNCH="upcxx-run -v -shared-heap 1G"
echo "Launching with: " ${LAUNCH}
export L_SIZE=31
echo "L = " ${L_SIZE}

export PROC=256
echo "PROC = " ${PROC}

echo ""
echo "==========================================================="
echo ""

echo "Run starts at $(date) on $(uname -n)"

$@



# A a problem size for Unagg
echo ${LAUNCH} -n 256 ./gup-unagg -l ${L_SIZE} -r 2  -c -e -v 2097152
${LAUNCH} -n 256 ./gup-unagg -l ${L_SIZE} -r 2  -c -e -v 2097152
# ${LAUNCH} -n 256 ./gup-unagg -l 30 -r 2 -c -e -v 2097152


# Won't run
# ${LAUNCH} -n 256 ./gup-unagg -l 29 -r 2 -c -e -v 2097152

# Unaggregated, takes 60 sec (140mups)
# echo ${LAUNCH} -n 256 ./gup-unagg -l ${L_SIZE} -r 2  -c -e -v 4194304
# ${LAUNCH} -n 256 ./gup-unagg -l ${L_SIZE} -r 2  -c -e -v 4194304

echo "Run ends at $(date) on $(uname -n)"
