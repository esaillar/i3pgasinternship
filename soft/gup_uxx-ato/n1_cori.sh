#!/bin/bash
#SBATCH --qos=debug
# #SBATCH --qos=regular
# #SBATCH --qos=overrun
# #SBATCH --time-min=00:30:00
#SBATCH -A m2878
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
#SBATCH -C haswell
# #SBATCH --constraint=haswell
#SBATCH --time=00:10:00
#SBATCH --job-name=N1_GUP_UNAGG
#
# Documentation:
# https://slurm.schedmd.com/documentation.html
# Slurm environment variables
# http://www.glue.umd.edu/hpcc/help/slurmenv.html


#SBATCH -o %x-%j.out   # output and error file name
                       #  (%j expands to jobID)
                       #  (%x expands to job name)
                       # "filename pattern" in https://slurm.schedmd.com/sbatch.html
#SBATCH --mail-user=baden@ucsd.edu
#SBATCH --mail-type=begin

#module load perftools-base perftools-lite
module list 

echo " === Environment:"
printenv 


# cat /proc/cpuinfo

echo ""
echo "==========================================================="
echo ""
lscpu
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


export LAUNCH="upcxx-run -v -shared-heap 256M"
echo "Launching with: " ${LAUNCH}
export L_SIZE=27
echo "L = " ${L_SIZE}
export GUP="./gup-unagg"
echo "GUP = " ${GUP}
echo ""
echo "==========================================================="
echo ""

echo "Run starts at $(date) on $(uname -n)"

$@


upcxx-run -n 32 ./gup-unagg -l 25 -r 2 -c -v 262144
upcxx-run -n 32 ./gup-agg -l 25 -r 2 -c -v 262144 -a 512

echo "Run ends at $(date) on $(uname -n)"
