#!/bin/sh
# Runs much slower so run with a smaller problem and for fewer repetitions
upcxx-run -n 8 ./gup-unagg -l 21 -r 2 -c -v 16384
upcxx-run -n 32 ./gup-unagg -l 25 -r 2 -c -v 16384
upcxx-run -n 32 ./gup-unagg -l 25 -r 2 -c -v 262144
# upcxx-run -n 2 ./gup-unagg -l 20 -r 2 -c -v 8192
# Aggregating version
# upcxx-run -n 8 ./gup-agg -l 23 -a 512 -r 5 -c
