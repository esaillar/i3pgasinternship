#!/bin/bash
#SBATCH --qos=debug
# #SBATCH --qos=regular
# #SBATCH --qos=overrun
# #SBATCH --time-min=00:30:00
#SBATCH -A m2878
#SBATCH --nodes=8
#SBATCH --ntasks-per-node=32
#SBATCH -C haswell
# #SBATCH --constraint=haswell
#SBATCH --time=00:30:00
#SBATCH --job-name=N8_GUP_RMA_UNAGG
#
# Documentation:
# https://slurm.schedmd.com/documentation.html
# Slurm environment variables
# http://www.glue.umd.edu/hpcc/help/slurmenv.html


#SBATCH -o %x-%j.out   # output and error file name
                       #  (%j expands to jobID)
                       #  (%x expands to job name)
                       # "filename pattern" in https://slurm.schedmd.com/sbatch.html
#SBATCH --mail-user=baden@ucsd.edu
#SBATCH --mail-type=begin

#module load perftools-base perftools-lite
module list 

echo " === Environment:"
printenv 


# cat /proc/cpuinfo

echo ""
echo "==========================================================="
echo ""
lscpu
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


export LAUNCH="srun -n"
echo "Launching with: " ${LAUNCH}
export PROC=256
echo "P factor = " ${PROC}


# export R=4
# export R=6
export R=2
echo "# reps = " ${R}

export K=2
echo "K factor = " ${K}

export V_SIZES=(4194304 8388608 16777216 33554432 67108864 134217728)
export V_SIZES=(4194304 8388608 16777216)
export V_SIZES=(4194304)
export PUT_V_SIZES=(4194304 8388608)
export VA_SIZES=(8388608 16777216)
export VA_SMALL=(1048576 2097152)
echo "V = " ${V_SIZES}

export L_SIZE=31
echo "L = " ${L_SIZE}

export A_VALS=(2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152)
export A_VALS=(256 512 1024 2048 4096 8192)
echo "A = " ${A_VALS}

echo ""
echo "==========================================================="
echo ""

echo "Run starts at $(date) on $(uname -n)"

$@

${LAUNCH} ${PROC} ./gup-notagg -l 29 -r 2 -v 2097152 -c -e 
#${LAUNCH} ${PROC} ./gup-notagg -l 31 -r 2 -v 8388608 -c -e 


echo "Run ends at $(date) on $(uname -n)"
