#!/usr/bin/bash
#SBATCH --nodes=4
#SBATCH --ntasks-per-node=32
#SBATCH --time=10:00:00
#SBATCH --job-name=FINAL_TEST-N4
#SBATCH -p routage
#SBATCH -C bora

#
# Documentation:
# https://slurm.schedmd.com/documentation.html
# Slurm environment variables
# http://www.glue.umd.edu/hpcc/help/slurmenv.html
# https://www.plafrim.fr/faq-en/

#SBATCH -o %x-%j.out   # output and error file name
                       #  (%j expands to jobID)
                       #  (%x expands to job name)
                       # "filename pattern" in https://slurm.schedmd.com/sbatch.html

# # SBATCH --mail-user=baden@ucsd.edu
# # SBATCH --mail-type=begin                                  
# set -e


module load compiler/gcc
module add compiler/intel
module load mpi/openmpi/4.0.2-testing
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/sbaden/bin/lib


echo " === Environment:"
printenv 

# cat /proc/cpuinfo

echo ""
echo "==========================================================="
echo ""
lscpu
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


export LAUNCH="mpirun"
echo "Launching MPI with: " ${LAUNCH}
echo ""
echo "==========================================================="
echo ""

echo "Run starts at $(date) on $(uname -n)"

$@


# for i in 4194304 8388608 16777216 33554432 67108864
# do
#     for j in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576
#     do
#         echo mpirun -np 32 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         mpirun -np 32 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         echo mpirun -np 32 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         mpirun -np 32 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         echo mpirun -np 32 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#         mpirun -np 32 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#     done
# done

# for i in 4194304 8388608 16777216 33554432 67108864
# do
#     for j in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576
#     do
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0 >> final_data/exp4/Mode0.txt
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1 >> final_data/exp4/Mode1.txt
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 800 -M 2
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 800 -M 2 >> final_data/exp4/Mode2.txt
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 4
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 4 >> final_data/exp4/Mode4.txt
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 8
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 8 >> final_data/exp4/Mode8.txt
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 16
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 16 >> final_data/exp4/Mode16.txt
#     done
#     echo mpirun -np 256 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#     mpirun -np 256 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0 >> final_data/exp4/Mode_AllPut.txt
# done

for i in 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30
do
    myVal=$((2**$(expr $i - 7)))
    echo mpirun -np 128 ./gup-agg -l $i -r 6 -c -v $myVal -a 512 -e -k 16 -M 0
    mpirun -np 128 ./gup-agg -l $i -r 6 -c -v $myVal -a 512 -e -k 16 -M 0 >> final_data/exp7/Mode_Agg.txt
    echo mpirun -np 128 ./gup-agg -l $i -r 6 -c -v $myVal -a 512 -e -k 16 -M 1
    mpirun -np 128 ./gup-agg -l $i -r 6 -c -v $myVal -a 512 -e -k 16 -M 1 >> final_data/exp7/Mode_AlltoAll.txt
done

for i in 14 15 16 17 18 19 20 21 22
do
    myVal=$((2**$(expr $i - 7)))
    # echo mpirun -np 128 ./gup-put -m 256 -l $i -r 6 -c -v 32768 -a 512 -e -k 1024 -M 0
    # mpirun -np 128 ./gup-put -m 256 -l $i -r 6 -c -v 32768 -a 512 -e -k 1024 -M 0 >> final_data/exp6/Mode_AllPut.txt
    echo mpirun -np 128 ./gup-base -l $i -r 6 -c -v $myVal -a 512 -e -k 256 -M 0
    mpirun -np 128 ./gup-base -l $i -r 6 -c -v $myVal -a 512 -e -k 256 -M 0 >> final_data/exp7/Mode_Base.txt
    echo mpirun -np 128 ./gup-loc -l $i -r 6 -c -v $myVal -a 512 -e -k 256 -M 0
    mpirun -np 128 ./gup-loc -l $i -r 6 -c -v $myVal -a 512 -e -k 256 -M 0 >> final_data/exp7/Mode_Loc.txt
done

# for i in 4194304 8388608 16777216 33554432 67108864 134217728
# do
#     for j in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152
#     do
#         echo mpirun -np 128 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         mpirun -np 128 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         echo mpirun -np 128 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         mpirun -np 128 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         echo mpirun -np 128 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#         mpirun -np 128 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#     done
# done

# for i in 4194304 8388608 16777216 33554432 67108864 134217728
# do
#     for j in 2 4 8 16 32 64 128 256 512 1024 2048 4096 8192 16384 32768 65536 131072 262144 524288 1048576 2097152
#     do
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 0
#         echo mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         mpirun -np 256 ./gup-agg -l 30 -r 6 -c -v $i -a $j -e -k 4 -M 1
#         echo mpirun -np 256 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#         mpirun -np 256 ./gup-unagg -l 30 -r 6 -c -v $i -a $i -e -k 4 -M 0
#     done
# done

echo "Run ends at $(date) on $(uname -n) with" ${SLURM_NNODES} "Nodes"
