//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"

#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object
// which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

//int alg_error_free = 0; // non-atomic explicit split-phase batching of reads and writes

// Used to report summary stats
extern int nentries;

extern int k_factor;

void Push(const int target,const int m_rank, const int k_expec, const int64 n_buff, const int size_buff, MPI_Win win, uint64* a_table, bool do_put = true){
    const int my_koff = m_rank * k_expec;
    assert(n_buff < k_expec);
    int64 offs_a = target * k_expec + n_buff;
    uint64 *pa = a_table + offs_a;
    *pa = size_buff - 1;

    if (do_put){
        MPI_Put(pa, size_buff, MPI_UINT64_T, target, my_koff + n_buff, size_buff, MPI_UINT64_T, win);
        MPI_Win_flush(target, win);
    }
}


// Version using aggregation factor
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
    alg_name = string("MPI-AGGREG-CLEAN");

    // Declare this locally
    ostringstream sl;

    int64 i, j;
    int x, y, z;
    int count = 0;
    const int kfac = k_factor;

    // Set up and distribute the table
    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    const uint64 expected_messages = Vlen / nranks; //expected number of messages to send to a target per batch
    const double fudge_factor = 1.3;
    const int n_slots = ceil((fudge_factor * ((double) (kfac * expected_messages)))/n_agg) + 1;
    const int k_expect = n_slots*n_agg;
    const int buff_size = k_expect * nranks;
    sl << " local table size: " << locTabSize << ", buffer size: " << buff_size << ", k_expect= " << k_expect << " Expected messages= " << expected_messages  << endl;
    sordst.OutputStream(sl);

    // Main table
    uint64 *table = new uint64[locTabSize];
    assert(table);

    uint64* u_table;

    // The update window
    MPI_Win upd;

    MPI_Win_allocate((MPI_Aint) buff_size * sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &u_table, &upd);

    // Table containing all the updates of a batch from the source process
    uint64* a = new uint64[buff_size];
    assert(a);

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks]; 

    // Counter to know how many messages had been sent to a target (for the version using aggregation factor)
    int* factor = new int[nranks];

    // Initialize main table, convert to local view index
    for(i = i0, j=0; i <= i1; i++, j++)
	table[j] = i;

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1; const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = ilog2(locTabSize);
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    comm->Barrier();
    int* ttarg = new int[nranks];

    int* num_mess = new int[nranks];
    for(int n = 0; n < nranks; n++){
        num_mess[n] = 0;
    }

    // beginning of the algorithm
    for (int r=0; r< nr; r++){

        rans = startr(start); // Initialize the random number generator

        comm->Barrier(); // Begin update timing here
        auto T0 = chrono::steady_clock::now();

        // Since not a collective call, we need barrier synchronization
        MPI_Win_lock_all(0,upd);
        comm->Barrier();
        for (i=0; i<nbatch; i++) {

            MPI_Win_flush_all(upd);
            comm->Barrier();
            for(x = 0; x < nranks; x++){ // Initialize the counter table
                counter[x] = 1;
                factor[x] = 0;
            }

            int too_many = 0;
            for (int k =0; k<nranks; k++) ttarg[k]=0;

            const int my_koff = myrank * k_expect;
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                
                if(targ != myrank){
                    if((1 + counter[targ]) >= k_expect){
                        // We only output the message the first
                        // time a given target would cause overflow
                        if (!ttarg[targ])
                            fprintf(stderr,"%sToo many messages from %d to %lld (counter=%d, k_expect=%d)%s\n", constants::YELLOW, myrank, (long long int) targ, counter[targ],k_expect, constants::BACK);
                        ttarg[targ]++; too_many++;
                    }
                    else{
                        a[targ * k_expect + counter[targ]] = rans;
                        counter[targ]++;
                    }

                    if(counter[targ] == (1 + factor[targ]) * n_agg){
                        const int64 n_buffered = factor[targ] * n_agg;
                        Push(targ, myrank, k_expect, n_buffered, n_agg, upd, a);
                        num_mess[targ]++;
//#define DEBUG 1
#ifdef DEBUG
                        if (targ == 2)
                            cout << constants::GREEN << "[" << myrank << "] sends " << n_agg-1 << " values -> " << targ << constants::BACK << endl;

#endif
                        factor[targ]++;
                        counter[targ]++; // Skip over count field of next buffer
                    }
                }
                else{ // if we are the target, we directly update our table 
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= rans; counter[targ]++;
                }
            }
//            sordst.OutputStream(sl);

            // Make sure that all of this rank's puts have completed
            MPI_Win_flush_all(upd); // No barrier needed

            // Do All Reduce to accumulate local error counts
            // All reduction might not acts as a barrier in some implementations
            int g_too_many = comm->gsum_int(too_many,true);
            comm->Barrier();
            if ((g_too_many > 0) && (!myrank))
                cerr << constants::RED << "** A total of " << g_too_many << " buffering ops resulted in overflow" << constants::BACK<< endl;

            // Everyone will execute this code or none at all
            if (g_too_many > 0) {
                comm->Barrier();
                char buffer[128];
                sprintf(buffer,"%sToo many messages!%s", constants::YELLOW, constants::BACK);

                FATAL_ERR_NO_RTN_EXIT(buffer);
            }

            //sending the remaining updates
            for(int t = 0; t < nranks; t++){
                int x = (myrank + t) % nranks;

                if(x != myrank){
                    const int64 n_buffered_remain = factor[x] * n_agg;
                    assert(n_buffered_remain < k_expect);
                    
                    int remain = counter[x] - n_buffered_remain;
                    assert(remain < n_agg);

                    Push(x, myrank, k_expect, n_buffered_remain, remain, upd, a,  remain > 1);
                    num_mess[x]++;
                    factor[x]++;
                }

                
            }

            MPI_Win_flush_all(upd);
            // Needed to ensure that all RMA has completed
            comm->Barrier();

            // Local updates
            for(int x = 0, xi=0; x < nranks; x++, xi+=k_expect){
                uint64 num_bis = 0; // Same role than factor but for the receiver
                if(x != myrank){
                    uint64 num = u_table[xi]; //counts the number of messages at the receiver
                    if (!num)
                        cout << "[" << myrank << "] num = 0 (xi=" << xi << ")" << endl;
                    assert(num <= n_agg);

                    // Clear the count for the next batch
                    u_table[xi] = 0;
                    bool last_one = num != n_agg-1;
                    while(true){
                        const int n0 = x * k_expect + num_bis * n_agg + 1;

                        for(y = n0; y < n0 + num; y++){
                            uint64 rans = u_table[y];
                            const int64 indxG = rans & tabszm1;
                            const uint64 offs = indxG & ltabszm1; // Determine offset
                            const uint64 ta =  indxG >> iTabSz;
                            assert(ta == myrank);
                            assert(offs < locTabSize); 
                            table[offs] ^= rans;
                        }
                        if (last_one){
                        // ~~~  Should clear the counter as below, though it shouldn't make a difference!
                            break;
                        }

                        num_bis++;
                        num = u_table[n0+n_agg-1];
                    //  Clear the count for the next batch 
                    // *** !!! Be sure that you do a flush_all and a barrier at the start
                    //         of the next window
                        u_table[n0+n_agg-1] = 0;
                        last_one = (num != n_agg-1);
                    }
                }
            }
        }
        MPI_Win_unlock_all(upd);

        // End update timed section
        comm->Barrier();
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a padded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used in this variant of Global Update
            // We use -1 to signify a dummy value for those arguments
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, dummyMode);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    sl << "+@, ";
    for (int k = 0; k < nranks; k++){
        //if(num_mess[k] > nr * (expected_messages / nranks))
            sl <<  num_mess[k] << ", ";
        //else
          //  sl << constants::BLUE << num_mess[k] << constants::BACK << ", ";
    }
    sl << endl;
    sordst.OutputStream(sl);

    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!myrank){
            if (!totErrors){
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl;
            }
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }

    comm->Barrier();

    MPI_Win_free(&upd);

    delete [] table;
    delete [] a;
    delete [] counter;
    delete [] ttarg;
    delete [] factor;
    delete [] num_mess;

    return totErrors;
}

