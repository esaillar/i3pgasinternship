//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"

#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object
// which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

// Used to report summary stats
extern int nentries;

// Every update file has to define this method
void init_AlgName(){
    alg_name = string("MPI-PUT");
}

// Version using only Puts
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
    
    // Declare this locally
    // ostringstream sl;

    int64 i, j;
    int res = 0;
    int x, y, z;
    int count = 0;
    int kfac = int (16);
    uint64 message;

    // Set up and distribute the table
    int64 i0, i1;
    int64 locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    // sl << "[" << myrank << "] about to allocate table and window" << endl;
    // Main table
    reportMemory("-table    ");
    uint64 *table = new uint64[locTabSize];
    for (int k=0; k < locTabSize; k++)
        table[k] = 0L;
    reportMemory("+table    ");

    // The window
    MPI_Win upd;

    const uint64 expected_messages = Vlen / nranks; //expected number of messages to send to a target per batch

    // The update window
    const int buff_size = kfac * expected_messages * nranks;
    reportMemory("-u_table  ");
    uint64 *u_table =  new uint64[buff_size];
    assert(u_table);
    // Initialize a to make sure it's paged into memory before we start
    for (int k=0; k < buff_size; k++)
        u_table[i] = 0L;
    reportMemory("+u_table  ");
    MPI_Win_create(u_table, buff_size * sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &upd);

    //replace nranks by nranks-1 for the version without RMA (rcv, a, counter)

    uint64 *rcv = new uint64[buff_size]; // local buffer to update our window

    // Table containing all the updates of a batch from the source process
    uint64* a = new uint64[buff_size];

    // Initialize a & rcv to make sure they're paged into memory before we start
    for (int k=0; k < buff_size; k++){
        a[i] = 0L;
        rcv[i] = 0L;
    }

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks]; 

    // Counter to know how many messages had been sent to a target (for the version using aggregation factor)
    int* factor = new int[nranks];

    // We don't optimze for the case that all the data are on one node
    // This is not the expected case

    // sl << "[" << myrank << "] Completed to allocation" << endl;
    // sordst.OutputStream(sl);

    comm->Barrier();

    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++){   //  converts to local view index
	table[j] = i;
    }

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = ilog2(locTabSize);
    // No Single node optimization, straight C++ 
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    // sl << "[" << myrank << "] Ready to do the iterations " << endl;
    // sordst.OutputStream(sl);
    // comm->Barrier();

    // beginning of the algorithm
    for (int r=0; r< nr; r++){
        // Restart the random number generator
        rans = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        for (i=0; i<nbatch; i++) {
            uint64 local_tab[Vlen];       // Current random numbers
            uint64 ran[Vlen];             // Current random numbers
            uint64 offset[Vlen], target[Vlen];
            uint64 boolean[Vlen];

            //reinitialize the counter table
            for(x = 0; x < nranks; x++){
               counter[x] = 0;
            }

            // filling the a array
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                
                if(targ != myrank){
                    if(1 + counter[targ] >= kfac * expected_messages){
                        printf("Too much messages from %d to %ld\n", myrank,
                                (long unsigned) targ);
                        exit(0);
                    }
                    else{
                        a[targ * (kfac * expected_messages) + 1 + counter[targ]] = rans;
                        counter[targ]++;
                    }
                }
                else{ // if we are the target, we directly update our table 
                    table[offs] ^= rans;
                    counter[targ]++;
                }
            }

            comm->Barrier();
            
            MPI_Win_lock_all(0,upd);
            for(x = 0; x < nranks; x++){
                if(x != myrank){  
                    a[x * kfac * expected_messages] = counter[x];
                    MPI_Put(a + x * kfac * expected_messages, counter[x] + 1, MPI_UINT64_T, x, myrank * kfac * expected_messages, counter[x] + 1, MPI_UINT64_T, upd);
                    }
            }
            MPI_Win_unlock_all(upd);

            comm->Barrier();

            // Local updates
            for(x = 0; x < nranks; x++){
                if(x != myrank){
                    uint64 num = u_table[x * kfac * expected_messages]; //counter of the number of messages
                    for(y = 1; y < 1 + num; y++){
                        uint64 rans = u_table[x * kfac * expected_messages + y];
                        const int64 indxG = rans & tabszm1;
                        const uint64 offs = indxG & ltabszm1;     // Determine offset
                        table[offs] ^= rans;
                    }
                }
            }
        }

        // End update timed section
        comm->Barrier();
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, dummyMode);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();
    reportMemory("+Done     ");


    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!myrank){
            if (!totErrors){
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl << "Number of messages: " << res << endl;
            }
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }

    comm->Barrier();

    //printf("Number of messages in process %d: %d\n", myrank, counter[0]);


    MPI_Win_free(&upd);

    delete [] rcv;
    delete [] u_table;
    delete [] table;
    //delete [] send;
    delete [] a;
    delete [] counter;
    delete [] factor;

    return totErrors;
}
