//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"

#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object
// which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

//int alg_error_free = 0; // non-atomic explicit split-phase batching of reads and writes

// Used to report summary stats
extern int nentries;

void init_AlgName(){
    alg_name = string("BASE");
}

//Base version

int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{

    

    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);

    void init_AlgName();

//    alg_name = string("BASE");
    
    // Declare this locally
    // ostringstream sl;
    
    int64 i, j;
    int res = 0;
    int x, y, z;
    int count = 0;
    int kfac = int (16);
    //uint64 *send = new uint64[Vlen];
    uint64 message;

    // Set up and distribute the table
    int64 i0, i1;
    int64 locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    // sl << "[" << myrank << "] about to allocate table and window" << endl;

    // Main table
    uint64 *table = new uint64[locTabSize];

    // The window
    MPI_Win win;
    MPI_Win_create(table,locTabSize*sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &win);

    const uint64 expected_messages = Vlen / nranks; //expected number of messages to send to a target per batch

    // We don't optimze for the case that all the data are on one node
    // This is not the expected case

    // sl << "[" << myrank << "] Completed to allocation" << endl;
    // sordst.OutputStream(sl);

    // comm->Barrier();

    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++){   //  converts to local view index
	table[j] = i;
    }

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = ilog2(locTabSize);
    // No Single node optimization, straight C++ 
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    // sl << "[" << myrank << "] Ready to do the iterations " << endl;
    // sordst.OutputStream(sl);
    // comm->Barrier();

    // beginning of the algorithm
    for (int r=0; r< nr; r++){
        // Restart the random number generator
        rans = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        MPI_Win_lock_all(0,win);
        for (i=0; i<nbatch; i++) {
            uint64 local_tab[Vlen];       // Current random numbers
            uint64 ran[Vlen];             // Current random numbers
            uint64 offset[Vlen], target[Vlen];
            uint64 boolean[Vlen];

            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target

                offset[j] = offs;
                target[j] = targ;
                ran[j] = rans;
                boolean[j] = 0;

                // update the remote values using accumulate RMA
                //MPI_Win_lock_all(0,win); //slowdown 2
                if(targ == myrank){
                    res++;
                }
                MPI_Accumulate(&rans, 1, MPI_UINT64_T, targ, offs, 1, MPI_UINT64_T, MPI_BXOR, win);
            }
            MPI_Win_flush_all(win);
//        comm->Barrier();
        }
        MPI_Win_unlock_all(win);
        // End update timed section
        comm->Barrier();
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, dummyMode);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();

    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!myrank){
            if (!totErrors){
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl << "Number of messages: " << res << endl;
            }
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }

    comm->Barrier();

    //printf("Number of messages in process %d: %d\n", myrank, counter[0]);


    MPI_Win_free(&win);

    delete [] table;
    
    return totErrors;
}
