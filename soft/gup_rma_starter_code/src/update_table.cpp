//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"

#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

// Used to report summary stats
extern int nentries;

extern int k_factor;

extern int Mod;

// Comment out this macro to introduce races
#define NO_RACE 1
int agg;

// Every module  needs to define this function so
// that other functions can call it.
void init_AlgName(){
    const bool All2All = Mod & 1;
    if (isAll2All())
        alg_name = string("MPI-A2A");
    else
        alg_name = string("MPI-AGG");
}

void Push_Put(const int target,const int m_rank, const int k_expec, const int64 n_buff, const int size_buff, MPI_Win win, uint64* a_table, bool do_put = true){
    const int my_koff = m_rank * k_expec;
    assert(n_buff < k_expec);
    int64 offs_a = target * k_expec + n_buff;
    uint64 *pa = a_table + offs_a;

    if (isSrcComplete()){
        pa = a_table + target * agg;
    }

    *pa = size_buff - 1;

    if (do_put){
        MPI_Put(pa, size_buff, MPI_UINT64_T, target, my_koff + n_buff, size_buff, MPI_UINT64_T, win);
        if (isFlushMode())
            MPI_Win_flush(target, win);
        else if (isFlushLocalMode() || isSrcComplete())
            MPI_Win_flush_local(target, win);
    }
}

void Push_Alltoall(const int m_rank, const int n_ranks, const int k_expec, int* counters, int* counters_rcv, uint64* a_table, uint64 *rcv_buffer){
    int* displacements = new int[n_ranks];
    assert(displacements);
    int* counts = new int[n_ranks];
    assert(counts);

    for(int x = 0; x < n_ranks; x++){

        if(x != m_rank){
            a_table[x * k_expec] = counters[x];
            displacements[x] = x * k_expec;
            counts[x] = counters[x]; // remove the +1 since counters is set to 1 in this version
        }
        else{
            displacements[x] = 0;
            counts[x] = 0;
        }
    }

    // To letting know each processor the number of elements they are going to receive 
    MPI_Alltoall(counts, 1, MPI_INT, counters_rcv, 1, MPI_INT, MPI_COMM_WORLD);
    comm->Barrier();

    MPI_Alltoallv(a_table, counts, displacements, MPI_UINT64_T, rcv_buffer, counters_rcv, displacements, MPI_UINT64_T, MPI_COMM_WORLD);

    comm->Barrier();
    delete [] displacements;
    delete [] counts;
}

int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);

//    const bool All2All = Mod & 1;

    init_AlgName();

    // Declare this locally
    ostringstream sl;

    int64 i, j;
    int x, y, z;
    int count = 0;
    // ** Fix this !!
    agg = n_agg;

    // Set up and distribute the table
    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    // Expected amount of data to send to a target per batch
    // Lower bound on the buffer size
    // We use the k-factor to adust the size of the buffer as needed

    const uint64 expected_messages = Vlen / nranks;


    int B = 0;
    if (isNewRule()){                   //    bool buffSizeRule = (Mod & 2) >> 1;
        B = n_agg;
        if (k_factor*n_agg < expected_messages){
            int newK = expected_messages/n_agg;
            const int ifudge = newK / 4;
            newK += ifudge;
            if (!myrank){
                cout << "Increasing kfactor from " << k_factor << " to " << newK << endl;
            }
            comm->Barrier();
            k_factor =  newK;
        }
    }
    else {
        if (!myrank)
            cout << "Using the old rule for sizing the buffer" << endl;
        comm->Barrier();
        B = expected_messages;
    }

    const int buff_size = B * nranks * k_factor;
    
    int k_expect;
    if (isAll2All()){
        k_expect = buff_size/nranks;
    }
    else{
        if (isNewRule())
            k_expect = n_agg*k_factor;
        else
            k_expect = buff_size/nranks;
    }
    sl << "   Local table size: " << locTabSize << ", buffer size: " << buff_size << endl << "   k_expect= " << k_expect << " Expected messages= " << expected_messages  << endl;
    sordst.OutputStream(sl);

    // Main table

    uint64 *table = new uint64[locTabSize];
    assert(table);

    uint64* u_table;

    // The update window
    MPI_Win upd;

    reportMemory("-WinAlloc ");
    MPI_Win_allocate((MPI_Aint) buff_size * sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &u_table, &upd);

    reportMemory("+WinAlloc ");
    // Initialize u_table to make sure it's paged into memory before we start
    for (int k=0; k < buff_size; k++)
        u_table[k] = 0L;
    reportMemory("+u_table  ");

    reportMemory("-AInit    ");
    // Table containing all the updates of a batch from the source process
    uint64* a;
    if(!isSrcComplete()) {
        a = new uint64[buff_size];
        assert(a);
        for (int k=0; k < buff_size; k++)
            a[k] = 0L;
    }
    else {
        a = new uint64[nranks * n_agg];
        assert(a);
        for (int k=0; k < nranks*n_agg; k++)
            a[k] = 0L;
    }

    reportMemory("+AInit    ");

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks];
    assert(counter);


    // Counter to know how many updates there are for the current buffer (for the Source completion version)
    int* temp_count = new int[nranks];
    assert(temp_count);

    // Counter to know how many messages had been sent to a target (for the version using aggregation factor)
    int* factor = new int[nranks];
    assert(factor);

    uint64 *rcv;
    int* counts_rcv;
    if (isAll2All()){
        rcv = new uint64[buff_size];
        assert(rcv);
        counts_rcv = new int[nranks];
        assert(counts_rcv);
    }

    // Initialize main table, convert to local view index
    reportMemory("-TableInit");
    for(i = i0, j=0; i <= i1; i++, j++)
	table[j] = i;
    reportMemory("+TableInit");

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1; const int64 ltabszm1 = locTabSize-1;
    const int64 offsMask = tabszm1 & ltabszm1;
    const int64 iTabSz = ilog2(locTabSize);
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    comm->Barrier();
    // Don't need this if doing All 2 All
    int* ttarg = new int[nranks];
    assert(ttarg);

    // beginning of the algorithm
    for (int r=0; r< nr; r++){

        rans = startr(start); // Initialize the random number generator

        comm->Barrier(); // Begin update timing here
        auto T0 = chrono::steady_clock::now();

        // Since not a collective call, we need barrier synchronization
        if(!isAll2All()) MPI_Win_lock_all(0,upd);
        comm->Barrier();
        for (i=0; i<nbatch; i++) {

            // Because we wrote into the shared buffer in the previous
            // iteration, we have to treat such writes as if the Puts
            // So, we are required to ensure source and remote completion
#ifdef NO_RACE
            if( !isAll2All() ){
                MPI_Win_flush_all(upd);
                comm->Barrier();
            }
#endif

            for(x = 0; x < nranks; x++){ // Initialize the counter table
                counter[x] = 1;
                factor[x] = 0;
                temp_count[x] = 1;
            }

            if (!isAll2All() ){
                // Clear out counter for tallying overflows
                int too_many = 0;
                for (int k =0; k<nranks; k++)
                    ttarg[k]=0;

                const int my_koff = myrank * k_expect;
                for (j=0; j<Vlen; j++) {
                    rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                    const int64 indxG = rans & tabszm1;       // Truncate to global table size
                    const uint64 targ =  indxG >> iTabSz;     // Determine target

                    if(targ != myrank){
                        // Report error if we overflow the buffer
                        // Output for only the first instance per target rank
                        // We'll summarize later on
                        if((1 + counter[targ]) >= k_expect){
                            if (!ttarg[targ])
                                fprintf(stderr,"%sBuffer overflow from %d to %lld (counter=%d, k_expect=%d)%s\n", constants::YELLOW, myrank, (long long int) targ, counter[targ],k_expect, constants::BACK);
                            ttarg[targ]++; too_many++;
                        }
                        else{
                            if(isSrcComplete()){
                                 a[targ * n_agg + temp_count[targ]] = rans;
                                 temp_count[targ]++;
                            }
                            else
                                a[targ * k_expect + counter[targ]] = rans;
                            counter[targ]++;
                        }

                        if ( (counter[targ] == (1 + factor[targ]) * n_agg) && !too_many){
                            const int64 n_buffered = factor[targ] * n_agg;
                            Push_Put(targ, myrank, k_expect, n_buffered, n_agg, upd, a);
                            factor[targ]++;
                            counter[targ]++; // Skip over count field of next buffer
                            temp_count[targ] = 1;
                        }
                    }
                    else{ // if we are the target, we directly update our table 
                        const uint64 offs = indxG & ltabszm1;     // Determine offset
                        table[offs] ^= rans; counter[targ]++;
                    }
                }

                // Make sure that all of this rank's puts have completed
                MPI_Win_flush_all(upd); // No barrier needed

                // Do All Reduce to accumulate local error counts
                // All reduction might not acts as a barrier in some implementations
                int g_too_many = comm->gsum_int(too_many,true);
                //
                // So we add an explicit barrier to ensure remote completion
                comm->Barrier();

                if (g_too_many>0){
                    int sum = 0;
                    for (int k = 0; k< nranks; k++){
                       sum+= ttarg[k];
                    }
                    sl << endl;
                    sl << "Target overflow count matrix. Count (rank)";
                    sordst.OutputStream(sl);
                    if (sum > 0){
                        sl << endl << myrank << ": ";
                        for (int k = 0; k< nranks; k++){
                            if (ttarg[k] > 0){
                                sl <<  ttarg[k] << " (" << k << ")";
                                if (k < (nranks-1))
                                    sl << ", ";
                                }
                        }
                    }
                    sordst.OutputStream(sl);
                    sl << endl;
                    sordst.OutputStream(sl);
                    sl << constants::RED << "** A total of " << g_too_many << " buffering ops resulted in overflow" << constants::BACK<< endl;
                    sl << endl;
                    sordst.OutputStream(sl);


                // Everyone will execute this code or none at all
                    comm->Barrier();
                    char buffer[128];
                    sprintf(buffer,"%sToo many messages!%s", constants::YELLOW, constants::BACK);
                    sl << buffer;
                    sordst.OutputStream(sl);
                    sl << endl;
                    sordst.OutputStream(sl);
                    comm->Barrier();

                    FATAL_ERR_NO_RTN_EXIT(buffer);
                }

                //sending the remaining updates
                for(int t = 0; t < nranks; t++){
                    int x = (myrank + t) % nranks;

                    if(x != myrank){
                        const int64 n_buffered_remain = factor[x] * n_agg;
                        assert(n_buffered_remain < k_expect);
                        
                        int remain = counter[x] - n_buffered_remain;
                        assert(remain < n_agg);

                        Push_Put(x, myrank, k_expect, n_buffered_remain, remain, upd, a,  remain > 1);
                        factor[x]++;
                    }
                }

                MPI_Win_flush_all(upd);
                // Needed to ensure remote completion at all targets
                comm->Barrier();

                // Local updates
                for(int x = 0, xi=0; x < nranks; x++, xi+=k_expect){
                    uint64 num_bis = 0; // Same role than factor but for the receiver
                    if(x != myrank){
                        uint64 num = u_table[xi]; //counts the number of messages at the receiver
                        if (!num)
                            cout << "[" << myrank << "] num = 0 (xi=" << xi << ")" << endl;
                        assert(num <= n_agg);

                        // Clear the count for the next batch
                        // We are modifying shared storage;
                        // we need to synchronize to avoid a data race
                        u_table[xi] = 0;
                        bool last_one = num != n_agg-1;
                        while(true){
                            const int n0 = x * k_expect + num_bis * n_agg + 1;

                            for(y = n0; y < n0 + num; y++){
                                uint64 rans = u_table[y];
                                // *** Take advantage of the 
                                //     distributive law: compute
                                //     tabszm1 & ltabszm1 outside the loop
//                                const int64 indxG = rans & tabszm1;
//                                const uint64 offs = indxG & ltabszm1; // Determine offset
                                const uint64 offs = rans & offsMask;
#ifdef DEBUG 
                                  const uint64 ta =  indxG >> iTabSz;
                                  assert(ta == myrank);
                                  assert(offs < locTabSize); 
#endif

                                table[offs] ^= rans;
                            }
                            if (last_one){
                            // ~~~  Should clear the counter as below, though it shouldn't make a difference!
                                // Beware: we are modifying shared storage!
                                u_table[n0+n_agg-1] = 0;
                                break;
                            }

                            num_bis++;
                            num = u_table[n0+n_agg-1];
                            // Beware: we are modifying shared storage!
                            u_table[n0+n_agg-1] = 0;
                            last_one = (num != n_agg-1);
                        }
                    }
                }
            }
            else{ // All2All
                for (j=0; j<Vlen; j++) {
                    rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                    const int64 indxG = rans & tabszm1;       // Truncate to global table size
                    const uint64 targ =  indxG >> iTabSz;     // Determine target
                    int too_many = 0;
                    if(targ != myrank){
                        if(1 + counter[targ] >= k_expect){
                            if (!ttarg[targ])
                                fprintf(stderr,"%sToo many messages from %d to %lld (counter=%d, k_expect=%d)%s\n", constants::YELLOW, myrank, (long long int) targ, counter[targ],k_expect, constants::BACK);
                            ttarg[targ]++; too_many++;
                        }
                        else{
                            a[targ * k_expect + counter[targ] + 1] = rans;
                            counter[targ]++;
                        }
                    }
                    else{ // if we are the target, we directly update our table
                        const uint64 offs = indxG & ltabszm1;     // Determine offset
                        table[offs] ^= rans;
                    }
                }

                Push_Alltoall(myrank, nranks, k_expect, counter, counts_rcv, a, rcv);

                // Local updates

                for(x = 0; x < nranks; x++){
                    uint64 num = rcv[x * k_expect]; //counter of the number of messages
                    for(y = 1; y < num; y++){
                        uint64 rans = rcv[x * k_expect + y];
                        const int64 indxG = rans & tabszm1;
                        const uint64 offs = indxG & ltabszm1;     // Determine offset
                        table[offs] ^= rans;
                    }
                }
            }
        }
        // This is required to avoid a race condition
        if(!isAll2All()) MPI_Win_unlock_all(upd);


        // End update timed section

        // If you take this barrier out, you introduce a race condition
        // because you are not allowed to write to window storage
        // using load store without flushing
        // Think  of it as doing an RMA operation
#ifdef NO_RACE
        comm->Barrier();
#endif
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();
//	reportMemory("+Done Iter");
        if (!myrank)
            cout << endl;
        comm->Barrier();

        // Bump the number of entries
        // Only if not a padded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used in this variant of Global Update
            // We use -1 to signify a dummy value for those arguments
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, Mod);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    sordst.OutputStream(sl);

#ifdef TARGET_COUNTS
    sl << endl;
    sl << "Target count matrix";
    sordst.OutputStream(sl);
    sl << endl;
#endif
    int max_puts = -1, min_puts = 1<<30;
    for (int k = 0; k< nranks; k++){

#ifdef TARGET_COUNTS
        sl <<  ((myrank == k) ? 0 : factor[k]);
        if (k < (nranks-1))
            sl << ", ";
#endif
        if (myrank != k){
            if (factor[k] > max_puts)
                max_puts=factor[k];
            if (factor[k] < min_puts)
                min_puts=factor[k];
        }
    }
#ifdef TARGET_COUNTS
    sordst.OutputStream(sl);
    sl << endl;
    sordst.OutputStream(sl);
#endif

    int all_max = comm->gmax_int(max_puts, true);
    int all_min = comm->gmin_int(min_puts, true);
    sordst.OutputStream(sl);
    sl << endl << "Global Max, Min puts : " << all_max << ", " << all_min << endl;
    sordst.OutputStream(sl);

    reportMemory("+Done     ");

    int64 totErrors = -1;

    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!totErrors)
            sl << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl;
        else
            sl <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        sordst.OutputStream(sl);
    }

    comm->Barrier();

    MPI_Win_free(&upd);

    delete [] table;
    delete [] a;
    delete [] counter;
    delete [] ttarg;
    delete [] factor;
    if (isAll2All()){
        delete [] rcv;
        delete [] counts_rcv;
    }
    delete [] temp_count;

    reportMemory("+Free     ");
    return totErrors;
}

