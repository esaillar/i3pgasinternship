//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"

#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object
// which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

//int alg_error_free = 0; // non-atomic explicit split-phase batching of reads and writes

// Used to report summary stats
extern int nentries;

extern int k_factor;


// Version using aggregation factor
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
    alg_name = string("MPI-AGGREG");

    // Declare this locally
    ostringstream sl;

    int64 i, j;
    int x, y, z;
    int count = 0;
//    const int kfac = int (2); 
    const int kfac = k_factor;

    // Set up and distribute the table
    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    const uint64 expected_messages = Vlen / nranks; //expected number of messages to send to a target per batch
    const double fudge_factor = 1.3;
    const int k_expect = floor(fudge_factor * ((double) (kfac * expected_messages)));
    const int buff_size = k_expect * nranks;
    sl << " local table size: " << locTabSize << ", buffer size: " << buff_size
        << ", k_expect=" << k_expect << " Expected messages=" << expected_messages  << endl;
    sordst.OutputStream(sl);

    // Main table
    uint64 *table = new uint64[locTabSize];
    assert(table);

    // Remote Buffer
    uint64 *update_table =  new uint64[buff_size];
    assert(update_table);
    for (int k=0; k< buff_size; k++) update_table[k] = 0;

    // The update window
    MPI_Win upd;

    // Create the window
    MPI_Win_create(update_table, (MPI_Aint) buff_size * sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &upd);


    // Table containing all the updates of a batch from the source process
    uint64* a = new uint64[buff_size];
    assert(a);

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks]; 

    // Counter to know how many messages had been sent to a target (for the version using aggregation factor)
    int* factor = new int[nranks];


    // Initialize main table, convert to local view index
    for(i = i0, j=0; i <= i1; i++, j++)
	table[j] = i;

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1; const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = ilog2(locTabSize);
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    comm->Barrier();

    // beginning of the algorithm
    for (int r=0; r< nr; r++){

        rans = startr(start); // Initialize the random number generator

        comm->Barrier(); // Begin update timing here
        auto T0 = chrono::steady_clock::now();
        MPI_Win_lock_all(0,upd);
        for (i=0; i<nbatch; i++) {

            // This did not HELP // comm->Barrier();
            for(x = 0; x < nranks; x++){ // Initialize the counter table
               counter[x] = 1;
               factor[x] = 0;
            }

            int too_many = 0; int* ttarg = new int[nranks];
            for (int k =0; k<nranks; k++) ttarg[k]=0;

            const int my_koff = myrank * k_expect;
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                
                if(targ != myrank){
                    if((1 + counter[targ]) >= k_expect){
                        // We only output the message the first
                        // time a given target would cause overflow
                        if (!ttarg[targ])
                            fprintf(stderr,"%sToo many messages from %d to %lld (counter=%d, k_expect=%d)%s\n", constants::YELLOW, myrank, (long long int) targ, counter[targ],k_expect, constants::BACK);
                        ttarg[targ]++; too_many++;
                    }
                    else{
                        a[targ * k_expect + counter[targ]] = rans;
                        counter[targ]++;
                    }

                    if(counter[targ] == (1 + factor[targ]) * n_agg){
                        const int64 n_buffered = factor[targ] * n_agg;
                        assert(n_buffered < k_expect);
                        uint64 offs_a  = targ * k_expect + n_buffered;
                        assert(offs_a < buff_size);
                        uint64 *pa = a + offs_a; *pa = n_agg - 1;
                        const int64 offset = my_koff + n_buffered;
                        assert(n_agg);

                        MPI_Put(pa, n_agg, MPI_UINT64_T, (int) targ, (MPI_Aint) offset, n_agg, MPI_UINT64_T, upd);
                        MPI_Win_flush(targ,upd);
//#define DEBUG 1
#ifdef DEBUG
                        if (targ == 2)
                            cout << constants::GREEN << "[" << myrank << "] sends " << n_agg-1 << " values -> " << targ << constants::BACK << endl;

#endif
                        factor[targ]++;
                        counter[targ]++; // Skip over count field of next buffer
                    }
                }
                else{ // if we are the target, we directly update our table 
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= rans; counter[targ]++;
                }
            }
            sordst.OutputStream(sl);
            // Make sure that all of this rank's puts have completed
            MPI_Win_flush_all(upd); // No barrier needed

            // Do All Reduce to accumulate local error counts
            // All reduction acts as a barrier
            int g_too_many = comm->gsum_int(too_many,true);
            if ((g_too_many > 0) && (!myrank))
                cerr << constants::RED << "** A total of " << g_too_many << " buffering ops resulted in overflow" << constants::BACK<< endl;

            // Everyone will execute this code or none at all
            if (g_too_many > 0) {
                comm->Barrier();
                char buffer[128];
                sprintf(buffer,"%sToo many messages!%s", constants::YELLOW, constants::BACK);

                FATAL_ERR_NO_RTN_EXIT(buffer);
            } 

            //sending the remaining updates
            for(int t = 0; t < nranks; t++){
                int x = (myrank + t) % nranks;

                if(x != myrank){
                    const int n_buffered_remain = factor[x] * n_agg;
                    assert(n_buffered_remain < k_expect);
                    int remain = counter[x] - n_buffered_remain;
                    assert(remain < n_agg);
                    assert(remain>0);
                    assert(remain>1);
                    uint64 ofs = x * k_expect + n_buffered_remain;
                    uint64 *pa_remain = a + ofs; *pa_remain = remain - 1;

                    const int64 offset = my_koff + n_buffered_remain;
                    MPI_Put(pa_remain, remain, MPI_UINT64_T, (int) x, (MPI_Aint) offset, remain, MPI_UINT64_T, upd);
                    MPI_Win_flush(x,upd); // No barrier needed
#ifdef DEBUG
                    if (x == 2)
                        cout << constants::BLUE << "[" << myrank << "] sends " << remain << " values -> " << x << constants::BACK << endl;
#endif
                    // ** Not necessary: the updated value will never be used
//                    factor[x]++;
                }
            }

            MPI_Win_flush_all(upd);
            // Needed to ensure that all RMA has completed
            comm->Barrier();

            // Local updates
            for(int x = 0, xi=0; x < nranks; x++, xi+=k_expect){
                uint64 num_bis = 0; // Same role than factor but for the receiver
                if(x != myrank){
                    uint64 num = update_table[xi]; //counts the number of messages at the receiver
                    if (!num)
                        cout << "[" << myrank << "] num = 0 (xi=" << xi << ")" << endl;
                    assert(num <= n_agg);

                    // Clear the count for the next batch
                    update_table[xi] = 0;
                    bool last_one = num != n_agg-1;
                    while(true){
                        const int n0 = x * k_expect + num_bis * n_agg + 1;
#ifdef DEBUG
                        if (myrank == 2)
                            cout << "[" << myrank << "] recvs " << num << " values <- " << x << " n0 = " << n0 << endl;
#endif
                        for(y = n0; y < n0 + num; y++){
                            uint64 rans = update_table[y];
                            const int64 indxG = rans & tabszm1;
                            const uint64 offs = indxG & ltabszm1; // Determine offset
                            table[offs] ^= rans;
                        }
                        if (last_one)
                            break;

                        num_bis++;
                        num = update_table[n0+n_agg-1];
                    // Clear the count for the next batch
                        update_table[n0+n_agg-1] = 0;
                        last_one = (num != n_agg-1);
                    }
                }
            }
//            sl << "[" << myrank << "] # buffers = " << num_bis << endl;
//            sordst.OutputStream(sl);
        }
        MPI_Win_unlock_all(upd);

        // End update timed section
        comm->Barrier();
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a padded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used in this variant of Global Update
            // We use -1 to signify a dummy value for those arguments
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, dummyMode);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // ** Not necessary: we execute a barrier at the end of each repetition
    // comm->Barrier();

    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!myrank){
            if (!totErrors){
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl;
            }
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }

    comm->Barrier();

    MPI_Win_free(&upd);

    delete [] update_table;
    delete [] table;
    delete [] a;
    delete [] counter;
    delete [] factor;

    return totErrors;
}
