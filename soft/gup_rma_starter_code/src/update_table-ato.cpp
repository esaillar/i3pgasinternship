//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"

#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object
// which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

int alg_error_free = 0; // non-atomic explicit split-phase batching of reads and writes

// Used to report summary stats
extern int nentries;


// Used to find the index of the target in the 2D array (we admit rank != myrank)
int target_index(int rank, int myrank){
    if(rank <= myrank) return rank;
    return rank - 1; 
}


int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
    alg_name = string("MPI-ATO");

    // Declare this locally
    ostringstream sl;

    int64 i, j;
    int res = 0;
    int x, y, z;
    int count = 0;
    int kfac = int (2);
    //uint64 *send = new uint64[Vlen];
    uint64 message;

    // Set up and distribute the table
    int64 i0, i1;
    int64 locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    // Main table
    uint64 *table = new uint64[locTabSize];


    // The window
    MPI_Win upd;
    //MPI_Win win;
    //MPI_Win_create(table,locTabSize*sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &win);

    const uint64 expected_messages = Vlen / nranks; //expected number of messages to send to a target per batch

    sl << "[" << myrank << "] about to allocate table and window" << endl;
    // The update window
    uint64 *update_table =  new uint64[kfac * expected_messages * nranks];
    MPI_Win_create(update_table, kfac * expected_messages * nranks * sizeof(uint64), sizeof(uint64), MPI_INFO_NULL, MPI_COMM_WORLD, &upd);

    //replace nranks by nranks-1 for the version without RMA (rcv, a, counter)

    uint64 *rcv = new uint64[kfac * expected_messages * nranks]; // local buffer to update our window 
    
    // Table to know were the data to send is and where to send in the target memory
    //int displacements[nranks];

    // Table to know the size of the buffer to send 
    //int counts[nranks];

    // Table to know the number of elements we receive
    //int counts_rcv[nranks];


    // Table containing all the updates of a batch from the source process
    uint64* a = new uint64[nranks * kfac * expected_messages];

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks]; 

    // Counter to know how many messages had been sent to a target (for the version using aggregation factor)
    int* factor = new int[nranks];
    
    // Maximum and Minimum of the number of updates that have been send for a target
    //int max = 0, min = Vlen;

    // We don't optimze for the case that all the data are on one node
    // This is not the expected case

    sl << "[" << myrank << "] Completed to allocation" << endl;
    sordst.OutputStream(sl);
    comm->Barrier();

    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++){   //  converts to local view index
	table[j] = i;
    }

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = ilog2(locTabSize);
    // No Single node optimization, straight C++ 
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    // Tables giving the beginning and the size of a principal table accessible by a processor  
    
    //int64 targetTabSize; 
    //int64 i0_target, i1_target;
    //int tar_off[nranks], tar_tab_size[nranks];  // gives the offset to access the target table
    /*
    for(x = 0; x < nranks; x++){
        Block(x, nranks, tabsize, i0_target, i1_target, targetTabSize);
        tar_off[x] = i0_target;
        tar_tab_size[x] = targetTabSize;
    }
    */
    sl << "[" << myrank << "] Ready to do the iterations " << endl;
    sordst.OutputStream(sl);
    comm->Barrier();

    // beginning of the algorithm
    for (int r=0; r< nr; r++){
        // Restart the random number generator
        rans = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        for (i=0; i<nbatch; i++) {
	  //min = Vlen; max = 0;
            //uint64 local_tab[Vlen];       // Current random numbers
            //uint64 ran[Vlen];             // Current random numbers
            //uint64 offset[Vlen], target[Vlen];
            //uint64 boolean[Vlen];


// Base version
/*
            MPI_Win_lock_all(0,win);
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target

                offset[j] = offs;
                target[j] = targ;
                ran[j] = rans;
                boolean[j] = 0;

                // update the remote values using accumulate RMA
                //MPI_Win_lock_all(0,win); //slowdown 2
                if(targ == myrank){
                    res++;
                }
                MPI_Accumulate(&rans, 1, MPI_UINT64_T, targ, offs, 1, MPI_UINT64_T, MPI_BXOR, win);
                //MPI_Win_unlock_all(win);
            }
            MPI_Win_unlock_all(win);
        }
*/

// Version with local reduce
/*
            for(x = 0; x < Vlen; x++){
                message = ran[x];
                count = 0;
                uint64 tar = target[x]; uint64 off = offset[x];
                if(!boolean[x]){
                    for(y = x + 1; y < Vlen; y++){
                        if(!boolean[y] && (target[y] == tar) && (offset[y] == off)){
                            send[count++] = ran[y];
                            boolean[y] == 1;
                        }
                    }
                
                    for(z = 0; z < count; z++){
                        message ^= send[z];
                    }

                    if(count > 0){
                        res++;
                        MPI_Accumulate(&message, 1, MPI_UINT64_T, tar, off, 1, MPI_UINT64_T, MPI_BXOR, win);
                    }
                    boolean[x] = 1;
                    //MPI_Accumulate(send, count, MPI_UINT64_T, tar, off, count, MPI_UINT64_T, MPI_BXOR, win);
                    //MPI_Accumulate(&message, 1, MPI_UINT64_T, tar, off, 1, MPI_UINT64_T, MPI_BXOR, win);
                }
            }
*/

// Version with a 1D array 
/*
            //printf("////////////////////////////////////////////////////////////////////////\n");
            uint64 *tab = new uint64[tabsize]();
            
            //Block(0, nranks, tabsize, i0_target, i1_target, targetTabSize);
            //tar_off[0] = 0;    


            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target

                offset[j] = offs;
                target[j] = targ;
                ran[j] = rans;

                //Block(myrank, nranks, tabsize, i0_target, i1_target, targetTabSize);
                // create a counter per target or create a 2D array

                //if(tar_off[targ] + offs < tabsize){
                tab[tar_off[targ] + offs] ^= rans;
                //}
                // else{
                //     if(!myrank){
                //         printf("ecart par rapport au max %ld\n", tar_off[targ] + offs - tabszm1);
                //         res++;
                //     }
                // }

                // update the remote values using accumulate RMA
                //res++;
                //MPI_Accumulate(&rans, 1, MPI_UINT64_T, targ, offs, 1, MPI_UINT64_T, MPI_BXOR, win);
            }
            //printf(".........................................................................\n");

            res++;

            MPI_Accumulate(tab, 8192, MPI_UINT64_T, 0, 0, 8192, MPI_UINT64_T, MPI_BXOR, win);  // why 8192???
            for(j = 1; j < nranks; j++){
                res++;
                MPI_Accumulate(tab + tar_off[j], 8192, MPI_UINT64_T, j, 0, 8192, MPI_UINT64_T, MPI_BXOR, win);
            }

            delete [] tab;
*/

// Without RMA (Alltoall)
/*
            // Creation of a 2D array
            // uint64** a = new uint64*[nranks - 1]; // no row for our rank
            
            // for(int x = 0; x < nranks - 1; x++)
            //   a[x] = new uint64[kfac*expected_messages](); // change the kfac with kfactor (then should cast into int)

            for(x = 0; x < nranks - 1; x++){ //reinitialize the counter table
               counter[x] = 0;
            }
            
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                const uint64 index = target_index(targ, myrank);
                
                if(targ != myrank){
                    if(1 + counter[index] >= kfac * expected_messages){
                       printf("Too much messages for target %ld from %d because counter[%ld] = %d à la boucle %ld\n", targ, myrank, index, counter[index], i);
                        exit(0);
                    }
                    else{
                        //a[index][1 + counter[index]] ^= rans; // the first element is to store the counter for the target
                        a[index * (kfac * expected_messages) + 1 + counter[index]] = rans;
                        counter[index]++;
                    }
                }
                else{ // if we are the target, we directly update our table 
                    table[offs] ^= rans;
                }
                // if(targ == myrank){
                    // res++;
                // }
            }
            
            
            for(x = 0; x < nranks; x++){
                if(x != myrank){ 
                    const uint64 index = target_index(x, myrank);
                    // a[index][0] = counter[index];
                    a[index * kfac * expected_messages] = counter[index];
                    displacements[x] = index * kfac * expected_messages;
                    counts[x] = kfac * expected_messages;
                    //res++;
                    //MPI_Put(a[index], counter[index] + 1, MPI_UINT64_T, x, x * kfac * expected_messages, counter[index] + 1, MPI_UINT64_T, win);
                }
                else{
                    displacements[x] = 0;
                    counts[x] = 0;
                }
            }
            //MPI_Win_unlock_all(win);

            res++;
            MPI_Alltoallv(a, counts, displacements, MPI_UINT64_T, rcv, counts, displacements, MPI_UINT64_T, MPI_COMM_WORLD);

            comm->Barrier();

            // Local updates

            for(x = 0; x < nranks; x++){
                uint64 index = target_index(x, myrank);
                uint64 num = rcv[index * kfac * expected_messages]; //counter of the number of messages
                for(y = 1; y < 1 + num; y++){
                    uint64 rans = rcv[index * kfac * expected_messages + y];
                    const int64 indxG = rans & tabszm1;
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= rans;
                }
            }

            // for(int x = 0; x < nranks - 1; x++) {
            //     delete [] a[x];
            // }
            // delete [] a;
            //         printf(".........................................................................\n");

        }
*/


// Without RMA (Alltoallv)
/*
            //reinitialize the counter table
            for(x = 0; x < nranks; x++){ 
               counter[x] = 0;
            }

            // filling the a array
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                
                if(targ != myrank){
                    if(1 + counter[targ] >= kfac * expected_messages){
                        printf("Too much messages from %d to %ld\n", myrank, targ);
                        exit(0);
                    }
                    else{
                        a[targ * (kfac * expected_messages) + 1 + counter[targ]] = rans;
                        counter[targ]++;
                    }
                }
                else{ // if we are the target, we directly update our table 
                    table[offs] ^= rans;
                }
                // if(targ == myrank){
                    // res++;
                // }
            }
            
            
            for(x = 0; x < nranks; x++){
                if(x != myrank){ 
                    a[x * kfac * expected_messages] = counter[x];
                    displacements[x] = x * kfac * expected_messages;
                    counts[x] = 1 + counter[x];
                }
                else{
                    displacements[x] = 0;
                    counts[x] = 0;
                }
            }

            // To letting know each processor the number of elements they are going to receive 
            MPI_Alltoall(counts, 1, MPI_INT, counts_rcv, 1, MPI_INT, MPI_COMM_WORLD);

            comm->Barrier();

            res++;
            MPI_Alltoallv(a, counts, displacements, MPI_UINT64_T, rcv, counts_rcv, displacements, MPI_UINT64_T, MPI_COMM_WORLD);

            comm->Barrier();

            // Local updates

            for(x = 0; x < nranks; x++){
                uint64 num = rcv[x * kfac * expected_messages]; //counter of the number of messages
                for(y = 1; y < 1 + num; y++){
                    uint64 rans = rcv[x * kfac * expected_messages + y];
                    const int64 indxG = rans & tabszm1;
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= rans;
                }
            }
        }
*/

// Version using only Puts
/*
            //reinitialize the counter table
            for(x = 0; x < nranks; x++){
               counter[x] = 0;
            }

            // filling the a array
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                
                if(targ != myrank){
                    //if(1 + counter[targ] >= kfac * expected_messages){
                        //printf("Too much messages from %d to %ld\n", myrank, targ);
                        //exit(0);
                    //}
                    //else{
                        a[targ * (kfac * expected_messages) + 1 + counter[targ]] = rans;
                        counter[targ]++;
                    //}
                }
                else{ // if we are the target, we directly update our table 
                   table[offs] ^= rans;
                    counter[targ]++;
                }
            }

            comm->Barrier();
            
            MPI_Win_lock_all(0,upd);
            for(x = 0; x < nranks; x++){
                if(x != myrank){  
                    a[x * kfac * expected_messages] = counter[x];
                    MPI_Put(a + x * kfac * expected_messages, counter[x] + 1, MPI_UINT64_T, x, myrank * kfac * expected_messages, counter[x] + 1, MPI_UINT64_T, upd);
                    }
                // if((counter[x] > max)){
                //     max = counter[x];
                // }
                // if(counter[x] < min){
                //     min = counter[x];
                // }
            }

            MPI_Win_unlock_all(upd);

            comm->Barrier();

            // Local updates
            for(x = 0; x < nranks; x++){
                if(x != myrank){
                    uint64 num = update_table[x * kfac * expected_messages]; //counter of the number of messages
                    for(y = 1; y < 1 + num; y++){
                        uint64 rans = update_table[x * kfac * expected_messages + y];
                        const int64 indxG = rans & tabszm1;
                        const uint64 offs = indxG & ltabszm1;     // Determine offset
                        table[offs] ^= rans;
                    }
                }
            }

            // if(myrank == 0){
            //     printf("%d, %d\n", min, max);
            // }
*/


// Version using aggregation factor

            //reinitialize the counter table
            for(x = 0; x < nranks; x++){
               counter[x] = 0;
               factor[x] = 0;
            }

            // filling the a array
            MPI_Win_lock_all(0,upd);
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 offs = indxG & ltabszm1;     // Determine offset
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                
                if(targ != myrank){
                    if(1 + counter[targ] >= kfac * expected_messages){
                        printf("Too much messages from %d to %ld\n", myrank, targ);
                        exit(0);
                    }
                    else{
                        a[targ * (kfac * expected_messages) + counter[targ]] = rans;
                        counter[targ]++;
                    }
                    if(counter[targ] > (1 + factor[targ]) * n_agg){
                        factor[targ]++;
                        MPI_Put(a + x * kfac * expected_messages + (factor[targ] - 1) * n_agg, n_agg, MPI_UINT64_T, targ, myrank * kfac * expected_messages + (factor[targ] - 1) * n_agg, n_agg, MPI_UINT64_T, upd);
                    }

                }
                else{ // if we are the target, we directly update our table 
                    table[offs] ^= rans;
                    counter[targ]++;
                }
            }
            MPI_Win_unlock_all(upd);

            comm->Barrier();

            //sending the remaining arrays
            MPI_Win_lock_all(0,upd);
            
            
            for(x = 0; x < nranks; x++){
                if(x != myrank){
                    int remain = counter[x] - factor[x] * n_agg;
                    a[x * kfac * expected_messages + counter[x] + 1] = counter[x]; // now we put the counter at the end of the array
                    factor[x]++;
                    MPI_Put(a + x * kfac * expected_messages + (factor[x] - 1) * n_agg, remain, MPI_UINT64_T, x, myrank * kfac * expected_messages + (factor[x] - 1) * n_agg, remain, MPI_UINT64_T, upd);
                }
            }

            MPI_Win_unlock_all(upd);

            comm->Barrier();

            // Local updates
            for(x = 0; x < nranks; x++){
                if(x != myrank){
                    uint64 num = update_table[x * kfac * expected_messages]; //counter of the number of messages
                    for(y = 1; y < 1 + num; y++){
                        uint64 rans = update_table[x * kfac * expected_messages + y];
                        const int64 indxG = rans & tabszm1;
                        const uint64 offs = indxG & ltabszm1;     // Determine offset
                        table[offs] ^= rans;
                    }
                }
            }


        }


        //printf("Number of messages in process %d: %d\n", myrank, res);

        // End update timed section
        comm->Barrier();
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, dummyMode);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();

    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!myrank){
            if (!totErrors){
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl << "Number of messages: " << res << endl;
            }
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }

    comm->Barrier();

    //printf("Number of messages in process %d: %d\n", myrank, counter[0]);


    //MPI_Win_free(&win);
    MPI_Win_free(&upd);

    delete [] rcv;
    delete [] table;
    //delete [] send;
    delete [] a;
    delete [] counter;
    delete [] factor;

    return totErrors;
}
