//
//  MPI-RMA implementation, a port of the upcxx-extras gups code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <assert.h>
#include <cmath>
#include <chrono>
#include <mpi.h>
#include "Comm.hpp"
#include <unistd.h>


#include "types.hpp"
#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"

// Includes the sordst object
// which has been initilized in main()
#include "OrderedStream.hpp"

using namespace std;

//int alg_error_free = 0; // non-atomic explicit split-phase batching of reads and writes

// Used to report summary stats
extern int nentries;

extern int k_factor;

void init_AlgName(){
    alg_name = string("MPI-A2A");
}

void Push(const int m_rank, const int n_ranks, const int k_expec, int* counters, int* counters_rcv, uint64* a_table, uint64 *rcv_buffer){
    int* displacements = new int[n_ranks];
    int* counts = new int[n_ranks];

    for(int x = 0; x < n_ranks; x++){

        if(x != m_rank){
            a_table[x * k_expec] = counters[x];
            displacements[x] = x * k_expec;
            counts[x] = 1 + counters[x];
        }
        else{
            displacements[x] = 0;
            counts[x] = 0;
        }
    }


    // To letting know each processor the number of elements they are going to receive 
    MPI_Alltoall(counts, 1, MPI_INT, counters_rcv, 1, MPI_INT, MPI_COMM_WORLD);
    comm->Barrier();

    // if(m_rank == 0){
    //     for(int i = 0; i < n_ranks; i++)
    //         printf("counters_rcv[%d] = %d\n counts[%d] = %d\n", i, counters_rcv[i], i, counts[i]);
    // }

    MPI_Alltoallv(a_table, counts, displacements, MPI_UINT64_T, rcv_buffer, counters_rcv, displacements, MPI_UINT64_T, MPI_COMM_WORLD);

    comm->Barrier();
    delete [] displacements;
    delete [] counts;
}


// Without RMA (Alltoallv)
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    int myrank, nranks;
    MPI_Comm_rank(MPI_COMM_WORLD,&myrank);
    MPI_Comm_size(MPI_COMM_WORLD,&nranks);
    init_AlgName();

    // Declare this locally
    ostringstream sl;

    int64 i, j;
    int res = 0;
    int x, y, z;
    int count = 0;
    int kfac = k_factor;
    // Set up and distribute the table
    int64 i0, i1;
    int64 locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);

    const uint64 expected_messages = Vlen / nranks; //expected number of messages to send to a target per batch
    const double fudge_factor = 1.3;
    const int n_slots = ceil((fudge_factor * ((double) (kfac * expected_messages)))/n_agg) + 1;
    const int k_expect = n_slots*n_agg;
    const int buff_size = k_expect * nranks;
    sl << " local table size: " << locTabSize << ", buffer size: " << buff_size << ", k_expect= " << k_expect << " Expected messages= " << expected_messages  << endl;
    sordst.OutputStream(sl);

    // sl << "[" << myrank << "] about to allocate table and window" << endl;

    // Main table
    uint64 *table = new uint64[locTabSize];
    assert(table);

    uint64 *rcv = new uint64[buff_size]; // local buffer to update our table 
    
    // Table to know were the data to send is and where to send in the target memory
    int* displacements = new int[nranks];

    // Table to know the size of the buffer to send 
    //int* counts = new int[nranks];

    // Table to know the number of elements we receive
    int* counts_rcv = new int[nranks];

    // Table containing all the updates of a batch from the source process
    uint64* a = new uint64[buff_size];

    // Counter to know how many updates there will be for the target rank
    int* counter = new int[nranks]; 

    // We don't optimze for the case that all the data are on one node
    // This is not the expected case

    // sl << "[" << myrank << "] Completed to allocation" << endl;
    // sordst.OutputStream(sl);

    comm->Barrier();

    // Initialize main table
    for(i = i0, j=0; i <= i1; i++, j++){   //  converts to local view index
	table[j] = i;
    }

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;

    const int64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;
    const int64 iTabSz = ilog2(locTabSize);
    // No Single node optimization, straight C++ 
    uint64 rans = startr(start);

    // If necessary, increment the number of repetitions to make it even
    int nr = nreps + (nreps%2);

    // sl << "[" << myrank << "] Ready to do the iterations " << endl;
    // sordst.OutputStream(sl);
    // comm->Barrier();
    int* ttarg = new int[nranks];

    // beginning of the algorithm
    for (int r=0; r< nr; r++){
        // Restart the random number generator
        rans = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        for (i=0; i<nbatch; i++) {

            //reinitialize the counter table
            for(x = 0; x < nranks; x++){ 
               counter[x] = 0;
            }

            int too_many = 0;
            for (int k =0; k<nranks; k++) ttarg[k]=0;

            // filling the a array
            for (j=0; j<Vlen; j++) {
                rans = (rans << 1) ^ ((int64) rans < 0 ? POLY : 0);
                const int64 indxG = rans & tabszm1;       // Truncate to global table size
                const uint64 targ =  indxG >> iTabSz;     // Determine target
                int too_many = 0;
                
                if(targ != myrank){
                    if(1 + counter[targ] >= k_expect){
                        if (!ttarg[targ])
                            fprintf(stderr,"%sToo many messages from %d to %lld (counter=%d, k_expect=%d)%s\n", constants::YELLOW, myrank, (long long int) targ, counter[targ],k_expect, constants::BACK);
                        ttarg[targ]++; too_many++;
                    }
                    else{
                        a[targ * k_expect + counter[targ] + 1] = rans;
                        counter[targ]++;
                    }
                }
                else{ // if we are the target, we directly update our table
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= rans;
                }
            }

            Push(myrank, nranks, k_expect, counter, counts_rcv, a, rcv);

            // Local updates

            for(x = 0; x < nranks; x++){
                uint64 num = rcv[x * k_expect]; //counter of the number of messages
                for(y = 1; y < num; y++){
                    uint64 rans = rcv[x * k_expect + y];
                    const int64 indxG = rans & tabszm1;
                    const uint64 offs = indxG & ltabszm1;     // Determine offset
                    table[offs] ^= rans;
                }
            }
        }

        // End update timed section
        comm->Barrier();
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg, freq, dummy_stats, doVerification, dummyMode);
            // Bump the number of entries
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();

    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize, nupdate, table, i0, i1, tabsize, errByRank);

        if (!myrank){
            if (!totErrors){
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl << "Number of messages: " << res << endl;
            }
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }

    comm->Barrier();

    //printf("Number of messages in process %d: %d\n", myrank, counter[0]);

    delete [] rcv;
    delete [] table;
    //delete [] send;
    //delete [] counts;
    delete [] counts_rcv;
    delete [] a;
    delete [] counter;
    delete [] ttarg;

    return totErrors;
}
