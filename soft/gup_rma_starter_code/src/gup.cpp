// Driver. Based in part on code specified below
//  Terms of use are as specified in UPC++_LICENSE.txt
//
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//   for GUPS benchmark rules
//
#include <iostream>
#include <iomanip>
#include <chrono>
#include <mpi.h>

#include "gup.hpp"
#include "utils.hpp"
#include "gup_utils.hpp"
#include "Comm.hpp"
#include "OrderedStream.hpp"

using namespace std;

void cmdLine(int argc, char *argv[], int& l_tabsize, int& m_update, int& nreps,
             int& Vlen, int& n_agg, int& kfactor, bool& printAff,
             bool& errorByRank, bool& doVerification, int& freq, int& Mode);

int reportAndCheck(const int Vlen, const int Alen, const int nreps,
                   int& kfactor, const int l_tabsize, const int m_update,
                   const bool printAffinity, int Mode);

void init_AlgName();

string alg_name; // algorithm name

// Access to generic IO interface
Comm *Comm::instance = 0;
Comm *comm = comm->getInstance();

// Set up ordered stream
OrderedStream sordst;

// Used for reporting summary statistics
double *wallTimes;
int nentries;

// Not used now, but will be useful with aggregation 
int64 *maxSlots;
int *maxSlotsHW;

int k_factor = 1;
int Mod = 0;


int main(int argc, char *argv[])
{
   TimeStamp();
   comm->Init(&argc, &argv);
   intrank_t myrank = comm->myRank();
   intrank_t nranks = comm->nRanks();
   init_AlgName();

   sordst.Init();
   ostringstream sl;


// Default parameter values
// Log of the size of main table (suggested: half of global memory)
    int l_tabsize = 25;

// Multiplier for # updates to table (suggested: 4x number of table entries)
    int m_update = 4;

// Launch window size
// If we specify V=1, we have just 1 batch
    int Vlen     = 1024;

// Progress frequency: used with UPC++, but not the MPI back-end
#ifdef UPCXX_VERSION
    int freq = 4;
#else
    int freq = -1;
#endif

// Aggregation factor (n_agg > 0): we aggregate in buffers of size Alen
// No aggegration right now, so n_agg = 0
    int n_agg = 0;

    int nreps = 1, kfactor=0, Mode=0;
    bool printAffinity = false, errByRank = false, doVerification = false;

// End setting up default parameter values

    cmdLine(argc, argv, l_tabsize, m_update, nreps, Vlen, n_agg, kfactor,
            printAffinity,errByRank,doVerification,freq, Mode);

    k_factor = kfactor;
    Mod = Mode;
    
    int ltabsize = l_tabsize;
    const int64 tabsize = ((int64) 1) << (int64) l_tabsize;
    const int64 nupdate = ((int64) m_update) * tabsize;
    
    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);

    // Check that the local table size is < 2^32
    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);
    int64 s = 1; s <<= 31;
    assert(locTabSize < s); 

    // Check and print parameters for run
    if (reportAndCheck(Vlen, n_agg, nreps, kfactor, l_tabsize, m_update, printAffinity, Mode))
        return(-1);

    // Initialize summary stats tables
    assert(wallTimes = new double[nreps]);
    assert(maxSlots = new int64[nreps]);
    assert(maxSlotsHW = new int[nreps]);
    nentries = 0;


//    sl << "Rank " << myrank << " calling update_table()\n";
    comm->Barrier();
    int64 totErrs = update_table(nreps, Vlen, n_agg, ltabsize,
                                 tabsize, nupdate, freq, errByRank,
                                 doVerification);
    //sl << "Rank " << myrank << " finished calling update_table()\n";

    // Flush the stream
    // May continue to do more stream output and call OutputStream again
    // as many times as desired
    // sordst.OutputStream(sl);

    comm->Barrier();
    // if (!myrank){
    //     cout << endl;
    //     cout << "<---< Run ends   at ";
    //     timestamp(true);
    // }

    if (nreps > 1)
        reportSummary(nreps, nupdate, Vlen, n_agg, false,doVerification,totErrs, Mode);

    comm->Barrier();

    // Closes the stream
    sordst.CloseStream();
    comm->Barrier();
    comm->Finalize();

    delete [] wallTimes;
    delete [] maxSlots;
    delete [] maxSlotsHW;

    return(0);
}
