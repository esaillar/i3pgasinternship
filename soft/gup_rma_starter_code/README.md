## Global Update (GUP) Starter code ##

GUP is a variant of the Global Update benchmark, AKA GUPS
We call it GUP, because it isn't identical to GUPS: we relax the constraint
on in-flight messages, and require that code be free from data races

For more information about GUPS, see this page

http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/

See cmdLine.cpp for various options that can be selected

The main flags for GUP are **-l -v**

Experiment with their values to get a feeling for the running time

Start with table sizes of 2<sup>20</sup> and progress to larger ones as you increase the number of cores.

The *-r* flag will repeat the run to enable performance data collection

The code will check correctness when the *-c* flag is asserted

The code requires that the number of repetitions specified
with -r be an even number.  If odd, GUP will run for an extra iteration.


A multithreaded version of the code is found in the public upcxx-extras repo,
which is located at [https://bitbucket.org/berkeleylab/upcxx-extras/src/master](https://bitbucket.org/berkeleylab/upcxx-extras/src/master)

## Building GUP ##

GUP uses files common located in various folders:

**src** holds the source files

**gup\_common** contains common files used by the app 

**../common** contains various utilities that aren't app specific

**obj** is the build directory, but the executable is installed in ./

The code should be built as a C++14 program 

To build the code, you need to define the envar MY\_BUILD\_ARCH to hold the
name of configuration file name that the Makefile includes from **../Arch** e.g.

MY\_BUILD\_ARCH = arch.gnu.linux-c++14.generic

MY\_BUILD\_ARCH = arch.intel-c++14.generic
