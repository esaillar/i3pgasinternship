This folder contains various sub-folders

**common** holds commonly used utilities, and is application-neutral

**gup\_starter\_code** holds the starter code for GUP, and is written in C++ with MPI RMA

**Arch** contains various profiles that are included by Makefile. Each installation will in general have its own version. We should be using config but that is future work.
