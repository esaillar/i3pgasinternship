#!/usr/bin/bash
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=32
#SBATCH --time=00:20:00
#SBATCH --job-name=GUP_REF_MPI_RMA-N1

# Should not normally change these
#SBATCH -p routage
#SBATCH -C bora

#
# Documentation:
# https://slurm.schedmd.com/documentation.html
# Slurm environment variables
# http://www.glue.umd.edu/hpcc/help/slurmenv.html
# https://www.plafrim.fr/faq-en/

#SBATCH -o %x-%j.out   # output and error file name
                       #  (%j expands to jobID)
                       #  (%x expands to job name)
                       # "filename pattern" in https://slurm.schedmd.com/sbatch.html

# Uncomment these two lines if you want to be notified by emil
# Be sure to fill in your email address
# #SBATCH --mail-user=<Your email address>
# #SBATCH --mail-type=begin
# set -e


# Should not normally change these
module load compiler/gcc
module add compiler/intel
module load mpi/openmpi/4.0.2-testing

echo " === Environment:"
printenv 

# cat /proc/cpuinfo

echo ""
echo "==========================================================="
echo ""

# Report information about the processor
lscpu


# Report job characteristics
echo ""
echo "Number of Nodes: " ${SLURM_NNODES}
echo "Node List: " $SLURM_NODELIST
export CORES_PER_NODE=${CORES_PER_NODE:=${SLURM_TASKS_PER_NODE%%\(*}}
export NTASKS=${NTASKS:=${SLURM_NTASKS}}
# echo "Number of cores/node: " ${SLURM_CPUS_ON_NODE}
echo "Detected CORES_PER_NODE=${CORES_PER_NODE} and NTASKS=${NTASKS}"
echo "my jobID: " $SLURM_JOB_ID
echo "Partition: " $SLURM_JOB_PARTITION
echo "submit directory:" $SLURM_SUBMIT_DIR
echo "submit host:" $SLURM_SUBMIT_HOST
echo "In the directory: `pwd`"
echo "As the user: `whoami`"


echo ""
echo "==========================================================="
echo ""

echo "Run starts at $(date) on $(uname -n)"

$@


mpirun  -n 32 ./aggy -l 25 -v 1048576 -a 512 -r 4 -c

echo "Run ends at $(date) on $(uname -n)"
