#include <stdio.h>
#include <iostream>
using namespace std;
#include "utils.hpp"

// https://www.linux.it/~rubini/docs/sysctl/

#if __APPLE__
#define SYSCTL_CORE_COUNT   "machdep.cpu.core_count"
#define CPU_SETSIZE 32
#include <sys/types.h>
#include <sys/sysctl.h>
typedef struct cpu_set {
  uint32_t    count;
} cpu_set_t;

static inline int
CPU_ISSET(int num, cpu_set_t *cs) { return (cs->count & (1 << num)); }

int sched_getaffinity(int pid, size_t cpu_size, cpu_set_t *cpu_set)
{
  int32_t core_count = 0;
  size_t  len = sizeof(core_count);
  int ret = sysctlbyname(SYSCTL_CORE_COUNT, &core_count, &len, 0, 0);
  if (ret) {
    printf("Error while getting core count %d\n", ret);
    return -1;
  }
  cpu_set->count = 0;
  for (int i = 0; i < core_count; i++) {
    cpu_set->count |= (1 << i);
  }

  return 0;
}
#else
#include <sched.h>
#endif

//Binding affinity Testing
#include <unistd.h>     //gethostname
#include <cstring>
//-----------------------------

/* Borrowed from util-linux-2.13-pre7/schedutils/taskset.c */
static char *cpuset_to_cstr(cpu_set_t *mask, char *str)
{
  char *ptr = str;
  int i, j, entry_made = 0;
  for (i = 0; i < CPU_SETSIZE; i++) {
    if (CPU_ISSET(i, mask)) {
      int run = 0;
      entry_made = 1;
      for (j = i + 1; j < CPU_SETSIZE; j++) {
        if (CPU_ISSET(j, mask)) run++;
        else break;
      }
      if (!run)
        sprintf(ptr, "%d,", i);
      else if (run == 1) {
        sprintf(ptr, "%d,%d,", i, i + 1);
        i++;
      } else {
        sprintf(ptr, "%d-%d,", i, i + run);
        i += run;
      }
      while (*ptr != 0) ptr++;
    }
  }
  ptr -= entry_made;
  *ptr = 0;
  return(str);
}

#include <sstream>
#include "OrderedStream.hpp"
void printCoreAffinity(int myrank){
  //2 lines binding thing
  cpu_set_t coremask;
  char clbuf[7 * CPU_SETSIZE], hnbuf[256];
  //-----------------------------
  memset(clbuf, 0, sizeof(clbuf));
  memset(hnbuf, 0, sizeof(hnbuf));
  (void)gethostname(hnbuf, sizeof(hnbuf));
  (void)sched_getaffinity(0, sizeof(coremask), &coremask);
  cpuset_to_cstr(&coremask, clbuf);
  std::ostringstream sl;
  sl << endl << "Core affinity" << endl;
  sordst.OutputStream(sl);
  sl << "   Rank " << myrank << " on  " << hnbuf << ". (Affin = " << clbuf << ")\n";
  sordst.OutputStream(sl);
  sl << endl;
  sordst.OutputStream(sl);
//  printf("Rank %d on %s. (Affin = %s)\n", myrank, hnbuf, clbuf);
  //-------------------------
}

const int MaxSize = 256;
void getStat(cpu_set_t& coremask, char *clbuf, char* hnbuf){
  //-----------------------------
  (void) gethostname(hnbuf, MaxSize);
  (void) sched_getaffinity(0, sizeof(coremask), &coremask);
  cpuset_to_cstr(&coremask, clbuf);
}

