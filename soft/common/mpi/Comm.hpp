#ifndef _Comm_hpp
#define _Comm_hpp

// https://www.tutorialspoint.com/Explain-Cplusplus-Singleton-design-pattern
// https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
// https://stackoverflow.com/questions/86582/singleton-how-should-it-be-used
#include <iostream>
#include <assert.h>
#include <mpi.h>
#include "utils.hpp"
#include "types.hpp"
using namespace std;

class Comm {
   static Comm *instance;
   int _myrank, _nranks;

   // Private constructor so that no objects can be created.
   Comm() {
      _myrank = -1;
      _nranks = -1;
   }

   public:
   static Comm *getInstance() {
      if (!instance){
         instance = new Comm;
      }
      return instance;
   }


   int Init(int *argc, char ***argv){
       int ierr = MPI_Init(argc,argv);
       MPI_Comm_rank(MPI_COMM_WORLD,&_myrank);
       MPI_Comm_size(MPI_COMM_WORLD,&_nranks);
       return(ierr);
   }

   int Finalize(){
      int ierr = MPI_Finalize();                                       \
      return(ierr);
   }

   int myRank() {
      return _myrank;
   }

   int nRanks() {
      return _nranks;
   }

   // The collectives can't be templated cleanly
   // becuase MPI requires constants for types

   // These collectives are not exhaustive,
   // I've defined only what I need

   int Barrier(){
       int ierr = MPI_Barrier(MPI_COMM_WORLD);
       return(ierr);
   }

   int bcast_int(int *vals, int n){
       int ierr = MPI_Bcast(vals,n,MPI_INT,0,MPI_COMM_WORLD);
       return(ierr);
   }

   int gsum_int64(int64 *inVals, int64 *gsum, int n, bool all=false){
       int ierr;
       if (all)
          ierr = MPI_Allreduce(inVals, gsum, n, MPI_INT64_T, MPI_SUM,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(inVals, gsum, n, MPI_INT64_T, MPI_SUM, 0, MPI_COMM_WORLD);
       return(ierr);
   }

   int gsum_int64(int64 inVal, bool all=false){
       int64 gsum = 0;
       int ierr;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gsum, 1, MPI_INT64_T, MPI_SUM,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gsum, 1, MPI_INT64_T, MPI_SUM, 0, MPI_COMM_WORLD);
       return(gsum);
   }

   int gsum_int(int inVal, bool all=false){
       int gsum = 0;
       int ierr;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gsum, 1, MPI_INT, MPI_SUM,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gsum, 1, MPI_INT, MPI_SUM, 0, MPI_COMM_WORLD);
       return(gsum);
   }

   int gmin_int(int inVal, bool all=false){
       int ierr;
       int gmin = 0;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gmin, 1, MPI_INT, MPI_MIN,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gmin, 1, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
       return(gmin);
   }

   //** SB Void
   int gmin_int(int *inVals, int *gmin, int n, bool all=false){
       int ierr;
       if (all)
          ierr = MPI_Allreduce(inVals, gmin, n, MPI_INT, MPI_MIN,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(inVals, gmin, n, MPI_INT, MPI_MIN, 0, MPI_COMM_WORLD);
       return(ierr);
   }

   int64 gmin_int64(int64 *inVals, int64 *gmin, int n, bool all=false){
       int ierr;
       if (all)
          ierr = MPI_Allreduce(inVals, gmin, n, MPI_INT64_T, MPI_MIN,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(inVals, gmin, n, MPI_INT64_T, MPI_MIN, 0, MPI_COMM_WORLD);
       return(ierr);
   }

   int64 gmin_int64(int64 inVal, bool all=false){
       int ierr;
       int64 gmin = 0;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gmin, 1, MPI_INT64_T, MPI_MIN,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gmin, 1, MPI_INT64_T, MPI_MIN, 0, MPI_COMM_WORLD);
       return(gmin);
   }

   int gmax_int(int inVal, bool all=false){
       int ierr;
       int gmax = 0;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gmax, 1, MPI_INT, MPI_MAX,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gmax, 1, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
       return(gmax);
   }

   int gmax_int64(int64 inVal, bool all=false){
       int ierr;
       int64 gmax = 0;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gmax, 1, MPI_INT64_T, MPI_MAX,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gmax, 1, MPI_INT64_T, MPI_MAX, 0, MPI_COMM_WORLD);
       return(gmax);
   }
   double gmax_double(double inVal, bool all=false){
       int ierr;
       double gmax = 0;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gmax, 1, MPI_DOUBLE, MPI_MAX,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gmax, 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD);
       return(gmax);
   }

   double gmin_double(double inVal, bool all=false){
       int ierr;
       double gmin = 0;
       if (all)
          ierr = MPI_Allreduce(&inVal, &gmin, 1, MPI_DOUBLE, MPI_MIN,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(&inVal, &gmin, 1, MPI_DOUBLE, MPI_MIN, 0, MPI_COMM_WORLD);
       return(gmin);
   }

   int gmax_int(int *inVals, int *gmax, int n, bool all=false){
       int ierr;
       if (all)
          ierr = MPI_Allreduce(inVals, gmax, n, MPI_INT, MPI_MAX,MPI_COMM_WORLD);
       else
          ierr = MPI_Reduce(inVals, gmax, n, MPI_INT, MPI_MAX, 0, MPI_COMM_WORLD);
       return(ierr);
   }

// *** Need to generate MPI types based on the type of T
//     Only works for T with a corresponding MPI Type
// We fudge the type here until we've worked out the type table
// https://docs.microsoft.com/en-us/message-passing-interface/mpi-datatype-enumeration
// https://pages.tacc.utexas.edu/~eijkhout/pcse/html/mpi-data.html#CC++
// https://www.geeksforgeeks.org/typeid-operator-in-c-with-examples/
void All2All_int(int *inVals, int *outVals){
       int ierr = MPI_Alltoall(inVals,1,MPI_INT, outVals, 1, MPI_INT, MPI_COMM_WORLD);
//       return(ierr);
   }
};
extern Comm *comm;
#endif
