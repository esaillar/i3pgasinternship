#ifndef _OrderedStream_
#define _OrderedStream_
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstdint>
#include <cstring>
#include <string>
#include "utils.hpp"
#include "Comm.hpp"

using namespace std;

const int K = 1024;

// OrderedStream is for debugging
// ** Use with care at scale, as this implementation scales poorly

class OrderedStream {
    private:
        MPI_Win _wins;
        char *sbuff;
        int MaxString = 16*K;

    public:
        OrderedStream(){
        }

        // This should be done once per program invocation

        void Init(){
            comm->Barrier();
            int nrpn = getNumRanksPerNode();

            // Only need to allocate nrpn*MaxString*sizeof(char) on ranks 1:p-1
            // where p = # ranks/node
            MPI_Win_allocate((MPI_Aint) MaxString*nrpn*sizeof(char), sizeof(char), MPI_INFO_NULL, MPI_COMM_WORLD, &sbuff, &_wins);
        }

        void CloseStream(){
            comm->Barrier();
            MPI_Win_free(&_wins);
        }

        void ClearStream(ostringstream& s1){
            s1.str("");
            s1.clear();
        }

        void OutputNodeZero(char *cstr, int len){
            intrank_t myrank = comm->myRank();
            intrank_t nranks = comm->nRanks();
            MPI_Win_lock_all(0,_wins);
            if (myrank>0)
                MPI_Put(cstr, len, MPI_CHAR, 0, (MPI_Aint) myrank*MaxString, len, MPI_CHAR, _wins);
            MPI_Win_flush_all(_wins);
            MPI_Win_unlock_all(_wins);
            comm->Barrier();
            if (!myrank){
                bool allIdentical = true;
                for (intrank_t i=1 ; i < nranks; i++){
                    if (strncmp(cstr, sbuff+i*MaxString,len-1) != 0)
                        allIdentical = false;
                }
                if (!allIdentical){
                    for (intrank_t i=1 ; i < nranks; i++){
                        cout << sbuff+i*MaxString;
                    }
                }
            }
            comm->Barrier();
        }
        void OutputNode(char *cstr, int len, int Node){
            intrank_t myrank = comm->myRank();
            intrank_t n = getNumRanksPerNode();
            MPI_Win_lock_all(0,_wins);
            if ((myrank>=Node*n) && (myrank < (Node+1)*n)){
                    if (myrank > 0){
                        // ** Beware the builtin constant
                        assert((myrank%n) < 32);
                        MPI_Put(cstr, len, MPI_CHAR, 0, (MPI_Aint) ((myrank%n)*MaxString), len, MPI_CHAR, _wins);
                    }
            }
            MPI_Win_flush_all(_wins);
            MPI_Win_unlock_all(_wins);
            comm->Barrier();

            if (!myrank){
                bool allIdentical = true;
                int cstr_emp = string(cstr).empty();

                for (intrank_t i=Node*n, j=0; i < (Node+1)*n; i++, j++){
                    if (i > 0){
                        if (string(sbuff+j*MaxString).empty()){
                            if (!cstr_emp)
                                allIdentical = false;

                        }
                        else{
                            if (cstr_emp)
                                allIdentical = false;
                            else
                                if (strncmp(cstr, sbuff+j*MaxString,len-1) != 0)
                                    allIdentical = false;
                        }
                    }
                }
                if (!allIdentical)
                    for (intrank_t i=Node*n, j=0; i < (Node+1)*n; i++, j++){
                        if (i > 0){
                            cout << sbuff+j*MaxString;
                        }
                    }
            }
            comm->Barrier();
        }

        void OutputStream(ostringstream& s1){

            string str = s1.str();
            int len = str.length()+1;

            char * cstr = new char [len];
            assert(cstr);
            strcpy (cstr, str.c_str());  // cstr now contains a c-string copy of str
            assert(len == (strlen(cstr)+1));

            int maxLen = comm->gmax_int(len, true);
            assert(maxLen <= MaxString);
            comm->Barrier();

            if (!comm->myRank()){
                cout <<  cstr;
            }

            // Rank 0 gets the output from all the other ranks, 1 node at a time
            int nnodes = nNodes();
            if (nnodes == 1){
//                OutputNodeZero(cstr,len);
                OutputNode(cstr, len, 0);
            }
            else{  // Output for nodes > 0
                for (int nd = 0; nd < nnodes ; nd++){
                    OutputNode(cstr, len, nd);
                }
            }

            delete[ ] cstr;
            ClearStream(s1);
        }
};
extern OrderedStream sordst;
        
#endif
