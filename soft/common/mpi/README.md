MPI specific common code go here

Aggregator.hpp     Aggregator code (to be written)
Comm.hpp           Generic interface to MPI that's duplicated
                   for each back-end, basic collectives, myrank, etc

OrderedStream.hpp  Ranks sorts the output so it isn't garbled
