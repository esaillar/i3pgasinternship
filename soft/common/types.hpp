#ifndef _types_h
#define _types_h

#ifndef UPCXX_VERSION
typedef int intrank_t;
#endif

#include <inttypes.h>
#include <utility>

using namespace std;

// 64 bit types

typedef uint64_t uint64;
typedef int64_t int64;

using int64x2 = pair<int64,int64>;
using intx2 = pair<int,int>;

#endif
