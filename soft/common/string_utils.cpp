// https://www.tutorialspoint.com/Explain-Cplusplus-Singleton-design-pattern
// https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
// https://stackoverflow.com/questions/86582/singleton-how-should-it-be-used

#include <assert.h>
#include <string>

using namespace std;

string c2str(const char* s){
    if (s == NULL)
        return(string(""));
    else
        return(string(s));
}
