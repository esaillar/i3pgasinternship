#ifndef _OrderedStream_
#define _OrderedStream_
#include <assert.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <cstdint>
#include <cstring>
#include <string>
#include "utils.hpp"
#include "Comm.hpp"

#include <upcxx/upcxx.hpp>
using namespace upcxx;
using namespace std;

const int K = 1024;

// OrderedStream is for debugging
// ** Use with care at scale, as this implementation scales poorly

class OrderedStream {
    private:
        global_ptr<char> sbuff = nullptr;
        int MaxString = 16*K;

    public:
        OrderedStream(){
        }

        // This should be done once per program invocation

        void Init(){
            comm->Barrier();
            int nrpn = getNumRanksPerNode();

            // Only need to allocate nrpn*MaxString*sizeof(char) on ranks 1:p-1
            // where p = # ranks/node
            if (!comm->myRank()){
                sbuff  = new_array<char>(MaxString*nrpn);
            }

            // After storage has been allocated, broadcast the pointer
            upcxx::barrier();
            sbuff = upcxx::broadcast(sbuff,0).wait();
        }

        void CloseStream(){
            comm->Barrier();
            if (!comm->myRank()){
                delete_array(sbuff);
            }
        }

        void ClearStream(ostringstream& s1){
            s1.str("");
            s1.clear();
        }

        void OutputNodeZero(char *cstr, int len, int maxLen){
            intrank_t myrank = comm->myRank();
            intrank_t nranks = comm->nRanks();
            if (myrank>0){
                rput(cstr,sbuff+myrank*maxLen,len).wait();
            }
            comm->Barrier();
            if (!myrank){
                bool allIdentical = true;
                assert(sbuff.is_local());
                char *sp = sbuff.local();
                for (intrank_t i=1 ; i < nranks; i++){
                    if (strncmp(cstr, sp+i*maxLen,len-1) != 0)
                        allIdentical = false;
                }
                if (!allIdentical){
                    for (int i=1 ; i < nranks; i++){
                        cout << sp+i*maxLen;
                    }
                }
            }
            comm->Barrier();
        }
        void OutputNode(char *cstr, int len, int maxLen, int Node){
            intrank_t myrank = comm->myRank();
            intrank_t n = getNumRanksPerNode();

            if ((myrank>=Node*n) && (myrank < (Node+1)*n)){
                    if (myrank > 0){
                        // ** Beware the builtin constant
                        assert((myrank%n) < 32);
                        rput(cstr,sbuff+(myrank%n)*maxLen,len).wait();
                    }
            }
            comm->Barrier();

            if (!myrank){
                bool allIdentical = true;
                assert(sbuff.is_local());
                char *sp = sbuff.local();
                for (intrank_t i=Node*n, j=0; i < (Node+1)*n; i++, j++){
                    if (i > 0){
                        if (strncmp(cstr, sp+j*maxLen,len-1) != 0)
                            allIdentical = false;
                    }
                }
                if (!allIdentical)
                    for (intrank_t i=Node*n, j=0; i < (Node+1)*n; i++, j++){
                        if (i > 0){
                            cout << sp+i*maxLen;
                        }
                    }
            }
            comm->Barrier();
        }

        void OutputStream(ostringstream& s1){

            string str = s1.str();
            int len = str.length()+1;

            char * cstr = new char [len];
            assert(cstr);
            strcpy (cstr, str.c_str());  // cstr now contains a c-string copy of str
            assert(len == (strlen(cstr)+1));

            int maxLen = comm->gmax_int(len,true);
            assert(maxLen <= MaxString);
            comm->Barrier();

            if (!comm->myRank()){
                cout <<  cstr;
            }

            // Rank 0 gets the output from all the other ranks, 1 node at a time
            int nnodes = nNodes();
            if (nnodes == 1){
//                OutputNodeZero(cstr,len,maxLen);
                OutputNode(cstr, len, maxLen, 0);
            }
            else{  // Output for nodes > 0
                for (int nd = 0; nd < nnodes ; nd++){
                    OutputNode(cstr, len, maxLen, nd);
                }
            }

            delete[ ] cstr;
            ClearStream(s1);
        }
};
extern OrderedStream sordst;
        
#endif
