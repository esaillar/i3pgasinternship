UPC++ specific parts here

Aggregator.hpp         Aggregator (to be written)

Comm.hpp               Generic interface to UPC++ that's duplicated
                       for each back-end, basic collectives, myrank, etc

OrderedStream.hpp      Ranks sorts the output so it isn't garbled
