#ifndef _Comm_hpp
#define _Comm_hpp
// https://www.tutorialspoint.com/Explain-Cplusplus-Singleton-design-pattern
// https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
// https://stackoverflow.com/questions/86582/singleton-how-should-it-be-used
#include <assert.h>
#include <upcxx/upcxx.hpp>
#include "../utils.hpp"
#include "types.hpp"
using namespace std;
using namespace upcxx;

class Comm {
    private:
    static Comm *instance;
    intrank_t _myrank, _nranks;

    global_ptr<int> inCounts;
    dist_object<global_ptr<int>> *pinCounts;
    global_ptr<int> *ldirectory;
    bool calledAll2All;

    // Private constructor so that no objects can be created.
    Comm() {
       _myrank = -1;
       _nranks = -1;
    }

    public:
    static Comm *getInstance() {
       if (!instance){
          instance = new Comm;
       }
       return instance;
    }

    // ** RETURN CODE?
    void Init(int *argc, char ***argv){

        upcxx::init();
        _myrank = upcxx::rank_me();
        _nranks = upcxx::rank_n();
        calledAll2All = false;
    }

   void Finalize(){
       // ** Delete storage that you allocated in upcxx
      if (calledAll2All){
          delete_array(inCounts);
          // Deleting dist_array is different
          delete  pinCounts;
          delete [ ] ldirectory;
      }
      upcxx::finalize();
   }

   intrank_t myRank() {
      return _myrank;
   }

   intrank_t nRanks() {
      return _nranks;
   }

   // ** Return code?
   void Barrier(){
       upcxx::barrier();
   }

   // ** Should these be int64s?
   void bCastInt(int *vals, int n){
       vals = upcxx::broadcast(vals,0).wait();
   }

   void gmin_int(int *inVals, int *gmin, int n, bool all=false){
       if (all)
           reduce_all(inVals, gmin, n, upcxx::op_fast_min).wait();
       else
          reduce_one(inVals, gmin, n, upcxx::op_fast_min,0).wait();
   }

   int gmin_int(int inVal, bool all=false){
       int gmin;
       if (all)
           gmin = reduce_all(inVal, upcxx::op_fast_min).wait();
       else
           gmin = reduce_one(inVal, upcxx::op_fast_min,0).wait();
       return(gmin);
   }

   int64 gmin_int64(int64 inVal, bool all=false){
       int64 gmin;
       if (all)
           gmin = reduce_all(inVal, upcxx::op_fast_min).wait();
       else
           gmin = reduce_one(inVal, upcxx::op_fast_min,0).wait();
       return(gmin);
   }


   void gmax_int(int *inVals, int *gmax, int n, bool all=false){
       if (all)
           reduce_all(inVals, gmax, n, upcxx::op_fast_max).wait();
       else
          reduce_one(inVals, gmax, n, upcxx::op_fast_max,0).wait();
   }

   int gmax_int(int inVal, bool all=false){
       int gmax;
       if (all)
           gmax = reduce_all(inVal, upcxx::op_fast_max).wait();
       else
           gmax = reduce_one(inVal, upcxx::op_fast_max,0).wait();
       return(gmax);
   }

   int64 gmax_int64(int64 inVal, bool all=false){
       int64 gmax;
       if (all)
           gmax = reduce_all(inVal, upcxx::op_fast_max).wait();
       else
           gmax = reduce_one(inVal, upcxx::op_fast_max,0).wait();
       return(gmax);
   }

   double gmax_double(double inVal, bool all=false){
       double gmax;
       if (all)
           gmax = reduce_all(inVal, upcxx::op_fast_max).wait();
       else
           gmax = reduce_one(inVal, upcxx::op_fast_max,0).wait();
       return(gmax);
   }

   double gmin_double(double inVal, bool all=false){
       double gmin;
       if (all)
           gmin = reduce_all(inVal, upcxx::op_fast_min).wait();
       else
           gmin = reduce_one(inVal, upcxx::op_fast_min,0).wait();
       return(gmin);
   }

   int64 gsum_int64(int64 inVal, bool all=false){
       int64 gsum = 0;
       if (all)
          gsum = reduce_all(inVal, upcxx::op_fast_add).wait();
       else 
          gsum = reduce_one(inVal, upcxx::op_fast_add,0).wait();
       return(gsum);
   }

   uint64 gsum_uint64(uint64 inVal, bool all=false){
       uint64 gsum = 0;
       if (all)
          gsum = reduce_all(inVal, upcxx::op_fast_add).wait();
       else 
          gsum = reduce_one(inVal, upcxx::op_fast_add,0).wait();
       return(gsum);
   }

// Basic all to all for scalars
// As the messages are short, we use a spanning tree like
// algorithm to distribute the data among the nodes.
// We aggregate data on the same node so that one represenative sends it

// In the first implementation none of these things
// and we use a naive ring algorithm to move the data
// This is not an efficient alogrithm, but it works

// In the next implementations, we'll add aggregation,
// and then the spanning tree formulation
// See these notes:
//   http://cseweb.ucsd.edu/classes/wi12/cse260-a/Lectures/Lec14.pdf
// We'll use recursive doubling

#ifdef TEMPLATED
    template<typename T>
    void All_to_All(T* in, T* out){
#endif
    // ** Ineffecient: we use a shared segment
    //    to receive the data, then copy into the argument
    // *** Maybe use an RPC instead?
    // *** Make this a templated function, we can use doubles, etc..

    void All2All_int(int* in, int* out){
        promise<> rputs_done;
        static bool first_time={true};
        calledAll2All = true;
        if (first_time){
            // ** BE SURE TO DELETE when done
            inCounts = new_array<int>(_nranks);
            pinCounts = new dist_object<global_ptr<int>>(inCounts);
            ldirectory = new global_ptr<int>[_nranks];
            // ** Probably use an offset to make this more scalable
            for (int i=0; i< _nranks; i++){
                ldirectory[i] = pinCounts->fetch(i).wait();
            }
            Barrier();
            first_time = false;
        }
    // Need to send count data to everyone else
    // *** One day we'll optimize this per notes above
        for (intrank_t t=0; t < _nranks; t++){
            intrank_t target = (_myrank + t ) % _nranks;
//            Put the data to the target
            global_ptr<int> dest = ldirectory[target] + _myrank;
            rput( in[target], dest, upcxx::operation_cx::as_promise(rputs_done));
        }
        rputs_done.finalize().wait();
        Barrier();
        int *inc = inCounts.local();
        assert(inc);
        // *** This is inefficient, to perform this last copy
        // If input were mapped to global segement, wouldn't need
        // to do this
        // *** Requires a (small) change to the app, however
        for (intrank_t t=0; t < _nranks; t++){
            out[t] = inc[t];
//          cout << " I[ " << _myrank << "]  :" << out[t] << endl;
        }
        Barrier();
    }

    void reportSegUsage(string mesg){
        int64 sh_seg_size = shared_segment_size();
        int64 max_seg_size  = gmax_int64(sh_seg_size, false);
        int64 min_seg_size  = gmin_int64(sh_seg_size, false);
        int64 sh_seg_used = shared_segment_used();
        int64 max_seg_used  = gmax_int64(sh_seg_used, false);
        int64 min_seg_used  = gmin_int64(sh_seg_used, false);

        if (!myRank()){
            cout << mesg << endl << "Max, Min shared segment size: " << max_seg_size << ", " << min_seg_size << endl;
            cout << "Max, Min shared segment used: " << max_seg_used << ", " << min_seg_used << endl;
        }
    }


    void report_uxx(){
        if (!myRank()){
            cout << "\nUPC++ Configuration"  << endl;
            cout << "UPC++ version: " << UPCXX_VERSION << endl;
            cout << "Spec  version: " << UPCXX_SPEC_VERSION << endl;

#ifdef UPCXX_THREADMODE
            cout << "Upcxx THREADMODE " << UPCXX_THREADMODE << endl;
#endif

#ifdef UPCXX_CODEMODE
            cout << "Upcxx CODEMODE" << UPCXX_CODEMODE << endl;
#endif

#ifdef UPCXX_NETWORK_IBV
            cout << "Network: InfiniBand OpenIB/OpenFabrics Verbs\n" << endl;
#endif

#ifdef UPCXX_NETWORK_ARIES
            cout << "Network: Aries (Cray XC)\n\n";
#endif

#ifdef UPCXX_NETWORK_UDP
            cout << "Nework: UDP (e.g. for Ethernet\n\n";
#endif

#ifdef UPCXX_NETWORK_SMP
            cout << "No network (single node)\n\n";
#endif
    #if UPCXX_ASSERT_ENABLED
            cout << "WARNING: UPCXX_CODEMODE=debug, performance results should not be trusted!" << endl;
    #endif
        }
    }
};
extern Comm *comm;
#endif
