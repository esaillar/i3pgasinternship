#ifndef _utils_hpp
#define _utils_hpp

#include <inttypes.h>
#include <sstream>
#include "types.hpp"

using namespace std;

string getJobString();
// string c2str(const char* s);
// void TimeStamp();
void TimeStamp(string Mesg=string(""));
void timestamp(bool newLine);
void printInfinitySign();
int getNumCores();
// uint32_t num_physical_cores();
int nNodes();
int getNumRanksPerNode();
int64 getPhysMem();
void reportMemory(string Mesg=string(""));

void Message(string mesg);
int64 q_factor(int64 n);

int ilog2(int val);
int64 ilog2(int64 val);

#define FATAL_ERR(message) do {                        \
    if (!(comm->myRank())) cerr << "ERROR: " << message << endl;   \
    comm->Finalize();                                   \
    return -1;                                           \
} while (0)

#define FATAL_ERR_NO_RTN(message) do {                        \
    if (!(comm->myRank())) cerr << "ERROR: " << message << endl;   \
    comm->Finalize();                                   \
} while (0)

#define FATAL_ERR_NO_RTN_EXIT(message) do {                        \
    if (!(comm->myRank())) cerr << "ERROR: " << message << endl;   \
    comm->Finalize();                                   \
    exit(1);                                            \
} while (0)

// Alterative assert assert
#define assertm(exp, msg) assert(((void)msg, exp))


#endif
