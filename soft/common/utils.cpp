#include <iomanip>
#include <iostream>
#include <fstream>
#include <math.h>
#include <stdint.h>
#include <chrono>
#include <assert.h>

#include "types.hpp"
#include "Comm.hpp"
#include "string_utils.hpp"
#include "utils.hpp"

#include "OrderedStream.hpp"

using namespace std;

string getJobString()
{
    const char* env_jobid = std::getenv("SLURM_JOBID");
    const string s_env_jobid = c2str(env_jobid);
    string job_string = "";

    if (s_env_jobid != ""){
        job_string += s_env_jobid ;
    }
    else{
        job_string += " -1";
    }
    char const* env_jobname = std::getenv("SLURM_JOB_NAME");
    const string s_env_jobname = c2str(env_jobname);

    if (s_env_jobname != ""){
        if (s_env_jobid != "")
            job_string += ", ";
        job_string += s_env_jobname;
    }
    return(job_string);
}


void TimeStamp( string Mesg){
    if (!comm->myRank()){
        if (Mesg.empty()){
            cout << endl << " ---> Timestamp: ";
            timestamp(true);
        } else{
            cout << endl << " ---> Timestamp [" << Mesg << "]: ";
            timestamp(true);
        }
    }
}

void timestamp(bool newLine){
    chrono::system_clock::time_point now = chrono::system_clock::now();
    time_t now_c = chrono::system_clock::to_time_t(now);
    cout << put_time(localtime(&now_c), "%F/%T");
    if (newLine)
       cout << endl;
}

void printInfinitySign(){
//     cout << "\u221E";
     cout << "INF";
}


#define sizeVal(_size) \
  ((_size) < (10ULL<<20) || (0) ? (((_size)>>9)+1)>>1 : (_size) < (10ULL<<30) ? (((_size)>>19)+1)>>1 : (_size) < (10ULL<<40) ? (((_size)>>29)+1)>>1 : (((_size)>>39)+1)>>1)
#define humanSize(_size) \
  ((_size) < (10ULL<<20) || (0) ? "KB" : (_size) < (10ULL<<30) ? "MB" : (_size) < (10ULL<<40) ? "GB" : "TB")

// ------
// Only works on linux systems
int64 get_free_mem() {
  string buf;
  ifstream f("/proc/meminfo");
  if (!f)
      return 0;
  int64 mem_free = 0;
  while (!f.eof()) {
    getline(f, buf);
    if (buf.find("MemFree") == 0 || buf.find("Buffers") == 0 || buf.find("Cached") == 0){
      stringstream fields;
      string units;
      string name;
      int64 mem;
      fields << buf;
      fields >> name >> mem >> units;
      if (units[0] == 'k') mem *= 1024L;
      mem_free += mem;
    }
  }
  f.close();
//  if (!comm->myRank()) cout << "MemFree = " << mem_free << endl;
  return mem_free;
}

#ifdef __APPLE_CC__
#include <sys/types.h>
#include <sys/sysctl.h>
#else
#include <sys/sysinfo.h>
#include <unistd.h>
#endif

int64 getPhysMem(){
#ifdef __APPLE_CC__
    int64 phys_mem= 0;
    size_t len = sizeof(phys_mem);
    int ierr = sysctlbyname("hw.memsize", &phys_mem, &len, 0, 0);
//    cout << "ierr, physmem: " << ierr << ", " << phys_mem << endl;
#else
//    ostringstream sl;
    // Returns the same result as sysconf
    // https://stackoverflow.com/questions/14386856/c-check-currently-available-free-ram/57659190#57659190
    long pageSize = sysconf(_SC_PAGESIZE);
    long npages = sysconf(_SC_PHYS_PAGES);
    long phys_mem = pageSize * npages;
//    sordst.OutputStream(sl);

#endif
    return (int64) phys_mem;
}

const double GB = 1024.0 *1024.0 *1024.0;
void reportMemory(string Mesg ){
    ostringstream sl;

#ifdef __APPLE_CC__
    int64 phys_mem = getPhysMem();
#else
    int64 phys_mem = get_free_mem();
#endif
    int64 availMem = comm->gmin_int64(phys_mem, true);
    double gb = floor( 100.0 * ((double) availMem) / GB)/100.0;
    sl << "[" << Mesg << "] Available Physical memory = " <<  gb  << " GB [" << availMem << "]" << endl;
    sordst.OutputStream(sl);

    return;

}
// ------

// https://www.tutorialspoint.com/Explain-Cplusplus-Singleton-design-pattern
// https://stackoverflow.com/questions/1008019/c-singleton-design-pattern
// https://stackoverflow.com/questions/86582/singleton-how-should-it-be-used
#include "string_utils.hpp"
using namespace std;

    int nNodes(){
        const char* c_env_nnodes = std::getenv("SLURM_JOB_NUM_NODES");
//        const char* c_env_nnodes = std::getenv("SLURM_NNODES");
        const string s_env_nnodes = c2str(c_env_nnodes);
        int n_nodes;
        // *** If we can't get the nodes from the environment
        // assume it's a single node
        if (s_env_nnodes == ""){
        //    cout << "Error: Can't get # Nodes from environment" << endl;
            n_nodes = 1;
        } else{ //  # Cores from environment
            n_nodes = atoi(c_env_nnodes);
        }
//        cout << "[[" <<  n_nodes << endl;
        return n_nodes;
    }


    int getNumRanksPerNode(){
        // If 1 node, 1 just return number of ranks
        if (nNodes() == 1){
            return(comm->nRanks());
        }
        // Else, multiple nodes, which are assumed to have a uniform # of cores
        else{
           const char* c_env_cname = std::getenv("SLURM_CLUSTER_NAME");
           const string s_env_cname = c2str(c_env_cname);
           assert(s_env_cname != "");
           const char* c_env_nersc = std::getenv("NERSC_HOST");
           const string s_env_nersc = c2str(c_env_nersc);
           int ranks_per_node;
           // NERSC (Cori for now)
           if (s_env_nersc != ""){
                  // More complicated formula:
                  // We divide # SLURM_PROCS by the number of nodes
                  const char* c_env_nprocs = std::getenv("SLURM_NPROCS");
                  const string s_env_nprocs = c2str(c_env_nprocs);
                  assert(s_env_nprocs != "");
                  const char* c_env_jnn = std::getenv("SLURM_JOB_NUM_NODES");
                  const string s_env_jnn = c2str(c_env_jnn);
                  assert(s_env_jnn != "");
                  ranks_per_node = atoi(c_env_nprocs)/atoi(c_env_jnn);
//                  return(atoi(c_env_nprocs)/atoi(c_env_jnn));
            }
           // just Plafrim for now, crash otherwise
            else{
// ##          assert(!s_env_cname.compare(string("plafrim")));
//               SLURM_TASKS_PER_NODE
               // Extract the # ranks per node from the environment
               // You need to find the right variable
               const char* c_env_cpu = std::getenv("SLURM_CPUS_ON_NODE");
               const string s_env_cpu = c2str(c_env_cpu);
	       // This is a kludge and only works for special cases
	       // It needs to be re-written to handle the general  case
	       // ${SLURM_TASKS_PER_NODE%%\(*}}
               if (s_env_cpu != ""){ // For plafrim
		   ranks_per_node = atoi(c_env_cpu);
	       } else{
		   ranks_per_node = 16;
	       }
//               return(atoi(c_env_cpu));
           }
//           Check that max = in
//           Return -1 processors if not
           int gcMax = comm->gmax_int(ranks_per_node, true);
           int gcMin = comm->gmin_int(ranks_per_node, true);
           if (gcMax != gcMin)
               return(-1);
           else
               return(ranks_per_node);
        }
    }

void Message(string mesg){
    cout << "[" << comm->myRank() << "]" << mesg << endl;
}

//
// Return a factor that's as close as possible to the square root of (n),
// where n is positive number and a power of two
//
// Return -1 otherwise

int64 q_factor(int64 n) {

    if (n<=0)
       return -1;

    int64 q = sqrt((double) n);
 
    int l2_p = (int) log2((float) q);
    int qp = pow(2.0,l2_p);
    if (q >  qp)
        q = qp;
    return (int) q;
}

// Adapted from OpenMPI
// http://www.open-mpi.org
// There is no specific attribution to this code.
// As there are many authors, refer to the above site

int ilog2(int val) {
  int i = -1;
  for( ; val != 0; val >>= 1, i++ );
  return i;
}

// 64 bit version of the OpenMPI int code
int64 ilog2(int64 val) {
  int i = -1;
  for( ; val != 0; val >>= 1, i++ );
  return i;
}
