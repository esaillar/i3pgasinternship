#ifndef _colors_hpp
#define _colors_hpp

using namespace std;

// https://en.wikipedia.org/wiki/ANSI_escape_code#Unix-like_systems

namespace constants{

const char BLACK[] = "\x1B[90m";
const char RED[] = "\x1B[91m";
const char GREEN[] = "\x1B[92m";
const char YELLOW[] = "\x1B[93m";
const char BLUE[] = "\x1B[94m";
const char MAGENTA[] = "\x1B[95m";
const char CYAN[] = "\x1B[96m";
const char WHITE[] = "\x1B[97m";
const char BACK[] = "\x1B[0m";
}

#endif
