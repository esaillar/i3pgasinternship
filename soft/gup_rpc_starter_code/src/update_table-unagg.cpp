// Copyright 2018, The Regents of the University of California
// Terms of use are as specified in LICENSE.txt
// Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//
//  RPC implementation
//
//

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <chrono>
#include <upcxx/upcxx.hpp>

#include "types.hpp"
#include "utils.hpp"
#include "OrderedStream.hpp"
#include "Comm.hpp"

#include "gup.hpp"
#include "gup_utils.hpp"

using namespace std;
using namespace upcxx;

extern int nentries;

// Main table
uint64 *table;
uint64 *targList;

// Ignore aggregation length n_agg for unaggregated version
int64 update_table(const int nreps, const int Vlen, const int n_agg,
                   const int ltabsize, const int64 tabsize,
                   const int64 nupdate, const int freq,
                   const bool errByRank, const bool doVerification)
{
    alg_name = string("UNAGG-UXX-RPC");
    intrank_t myrank = comm->myRank();
    intrank_t nranks = comm->nRanks();

    int64 i0, i1, locTabSize;
    Block(myrank, nranks, tabsize, i0, i1, locTabSize);
    table = new uint64[locTabSize];
    assert(table);

    // Initialize main table
    for(int64 i = i0, j=0; i <= i1; i++, j++)         // converts to local index
	table[j] = i;

    targList = new uint64[nranks];
    assert(targList);
    for (intrank_t i = 0; i < nranks; i++)
        targList[i] = 0;

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);
    const int64 nbatch = size/Vlen;
    uint64  ran = startr(start);
    ostringstream sl4;

    const int64 tabszm1 = tabsize-1;
    const int64 iTabSz = log2(locTabSize);
    const int64 ltabszm1 = locTabSize-1;
    const int   freqm1 = freq-1;

    // We add an extra iteration if nreps is odd; this enables us
    // to use reversibilty property of the updates to perform rapid verification
    int nr = nreps + (nreps%2);

    for (int r=0; r< nr; r++){

        // Restart the random number generator
        ran = startr(start);

        // Begin update timing here
        comm->Barrier();
        auto T0 = chrono::steady_clock::now();

        for (int k=0; k<nbatch; k++) {

            // Round-trip RPC variant synchronized using promises
            // The promise object counts the number of outstanding ops
            promise<> prom;
            for (int j=0; j<Vlen; j++) {

                ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
                const int64 indxG = ran & tabszm1;         // Truncate to global table size
                const uint64 targ = indxG >> iTabSz;       // The target
                const uint64 offs = indxG & ltabszm1;      // The offset

                if (targ == myrank){ // Inline the call
                    table[offs] ^= ran;
                }
                else{
                    targList[targ]++;
                    rpc(targ,upcxx::operation_cx::as_promise(prom),
                        [](const int64 idx, uint64 r){table[idx] ^= r;},
                        offs, ran);
                }

                if (!(j & freqm1))                   // freq must be a power of 2
                    progress();
            }

            // Return a future when all the promises have been fulfilled
            // Counter reaches zero
            future<> fut = prom.finalize();
            fut.wait();
        }

        // Barrier ensures global quiescence before verification begins
        // Otherwise we could get verification failures with sufficient load
        // imbalance, and/or contamination of the timed region with concurrent
        // verification code
        comm->Barrier();

        // End update timed section
        auto T1 = chrono::steady_clock::now();
        chrono::duration<double> t_walltime = T1-T0;
        double walltime = t_walltime.count();

        // Bump the number of entries
        // Only if not a paded iteration (see comment above)
        if ( (nreps == nr) || ( (nreps < nr) && (r<nreps)) ) {
            // Some arguments aren't used
            // We will use them in other implementations
            // We use -1 to signify a dummy value that isn't to be used
            stats_tuple dummy_stats = make_pair(make_pair(-1,-1),make_pair(-1,-1));
            int dummyMode = -1;
            reportResults(walltime, nupdate, ltabsize, Vlen, n_agg,
                          freq, dummy_stats, doVerification, dummyMode);
            assert(nentries <= nreps);
            nentries++;
        }
    }

    // This barrier ensures quiescence
    // May not be necessary since we did synchronized at a
    // fence at the end of the last batch
    comm->Barrier();

    // Verification
    int64 totErrors = -1;
    if (doVerification){
        totErrors = Verify(locTabSize,nupdate,table, i0, i1, tabsize, errByRank);
        if (!myrank){
            if (!totErrors)
                cout << endl << constants::YELLOW << "CORRECT!" << constants::BACK << endl;
            else
                cout <<  endl << constants::RED <<  "*** " << totErrors << " errors" << constants::BACK <<  endl;
        }
    }
#ifdef SHOW_COUNTS
    sordst.ClearStream();
    sl4 << "+@, ";
    for (intrank_t k = 0; k< nranks; k++)
        sl4 <<  targList[k]  << ", ";
    sl4 << endl;
#endif

    sordst.OutputStream(sl4);
    delete [] table;
    delete [] targList;
    return totErrors;
}
