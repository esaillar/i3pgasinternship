# This Makefile demonstrates the recommended way to build simple UPC++ programs.
# Note this uses some GNU make extensions for conciseness.
#
# If the UPCXX_INSTALL environment variable is set, this Makefile will use the
# upcxx compiler installed under that installation directory. If it is not set,
# this Makefile assumes that upcxx is in the user's PATH.
#
# UPCXX_INSTALL can either be set globally in your environment (e.g. inside
# .bashrc), or on invocation of this script, e.g.
#
# make UPCXX_INSTALL=<myinstalldir> all
# or
# export UPCXX_INSTALL=<myinstalldir>; make all
#

ENV = env UPCXX_THREADMODE=$(UPCXX_THREADMODE) UPCXX_CODEMODE=$(UPCXX_CODEMODE)
ifeq ($(UPCXX_INSTALL),)
$(warning UPCXX_INSTALL environment variable is not set, assuming upcxx is in the PATH)
UPCXX= $(ENV) upcxx
UPCXXRUN=upcxx-run
else
UPCXX= $(ENV) $(UPCXX_INSTALL)/bin/upcxx
UPCXXRUN=$(UPCXX_INSTALL)/bin/upcxx-run
endif

# --------------------------------------------------------
#  Settings, with defaults:

UPCXX_THREADMODE ?= seq
export UPCXX_THREADMODE
#UPCXX_CODEMODE ?= debug
UPCXX_CODEMODE ?= O3
export UPCXX_CODEMODE

# optional additional command-line arguments
CXXFLAGS ?=
CPPFLAGS ?=
LDFLAGS ?=
LIBFLAGS ?=

# CPPFLAGS += -I$(MY_BIN)/include
# LIBFLAGS += -L$(MY_BIN)/lib -lhwloc
LIBFLAGS += -lhwloc

# end code building flags

# --- Definition of source and search path directories

# Sources come from 3 different places
# ../common     ../common/upcxx		./src	
# Headers come from the above places + these header-only files
# ../../common/upcxx

# Source code for the app
SRC_DIR := ./src

# Generic common code for all apps
SRC_DIR_COMMON := ../common

# Common Code for the app
SRC_DIR_COMMON_GUP := ../gup_common

# Generic functionality across MPI and UPC++
# For UPC++ code we refer to the UPC++ version
SRC_DIR_COMM_COMMON := $(SRC_DIR_COMMON)/upcxx

OBJ_DIR := ./obj
OBJ_DIR_UNAGG := ./obj
OBJ_DIR_AGG := ./obj
OBJ_DIR_COMMON := ./obj
OBJ_DIR_COMMON_GUP := ./obj

SRC_FILES_AGG := $(wildcard $(SRC_DIR)/*.cpp)
SRC_FILES_AGG := $(filter-out $(SRC_DIR)/update_table-unagg.cpp,$(SRC_FILES_AGG))

SRC_FILES_UNAGG := $(wildcard $(SRC_DIR)/*.cpp)
SRC_FILES_UNAGG := $(filter-out $(SRC_DIR)/update_table-agg.cpp, $(SRC_FILES_UNAGG))

OBJ_FILES_AGG := $(SRC_FILES_AGG:.cpp=.o)
OBJ_FILES_AGG := ${subst $(SRC_DIR),$(OBJ_DIR),$(OBJ_FILES_AGG)}

OBJ_FILES_UNAGG := $(SRC_FILES_UNAGG:.cpp=.o)
OBJ_FILES_UNAGG := ${subst $(SRC_DIR),$(OBJ_DIR),$(OBJ_FILES_UNAGG)}

SRC_FILES_COMMON_GUP := $(wildcard $(SRC_DIR_COMMON_GUP)/*.cpp)
OBJ_FILES_COMMON_GUP := $(patsubst $(SRC_DIR_COMMON_GUP)/%.cpp,$(OBJ_DIR_COMMON_GUP)/%.o,$(SRC_FILES_COMMON_GUP))

SRC_FILES_COMMON := $(wildcard $(SRC_DIR_COMMON)/*.cpp)
SRC_FILES_COMMON := $(filter-out $(SRC_DIR_COMMON)/vFind.cpp, $(SRC_FILES_COMMON))
OBJ_FILES_COMMON := $(patsubst $(SRC_DIR_COMMON)/%.cpp,$(OBJ_DIR_COMMON)/%.o,$(SRC_FILES_COMMON))

COMMON := $(SRC_DIR_COMMON) $(SRC_DIR_COMMON_GUP) $(SRC_DIR_COMM_COMMON)
INCLUDE_FLAGS = $(COMMON:%=-I%)
CPPFLAGS += $(INCLUDE_FLAGS)

# We only have an unaggregatged version to start with
VARIANTS = unagg
#VARIANTS = agg unagg
TARGETS :=  $(VARIANTS:%=gup-%)

DEPS := Makefile .options
all: directories $(TARGETS)

# Forces the code to be re-built if you change any of the following
# since the last compilation (history stored in the hidden file .options):

OPTIONS = $(UPCXX_INSTALL) $(UPCXX_GASNET_CONDUIT) $(UPCXX_THREADMODE) $(UPCXX_CODEMODE) $(CXXFLAGS) $(CPPFLAGS) $(LDFLAGS) $(LIBFLAGS)
.options: force
	@echo '$(OPTIONS)' | cmp -s - $@ || (set -x ; echo '$(OPTIONS)' > $@ )

# Provided Unaggregated version
OBJ_UNAGG = $(OBJ_FILES_UNAGG) $(OBJ_FILES_COMMON_GUP) $(OBJ_FILES_COMMON)

# Aggregated version derived from the unaggregated code
OBJ_AGG = $(OBJ_FILES_AGG) $(OBJ_FILES_COMMON_GUP) $(OBJ_FILES_COMMON)

gup-unagg: $(OBJ_UNAGG)
	$(UPCXX) $(CXXFLAGS) $(LDFLAGS) $? $(LIBFLAGS) -o $@

gup-agg: $(OBJ_AGG)
	$(UPCXX) $(CXXFLAGS) $(LDFLAGS) $? $(LIBFLAGS) -o $@

$(OBJ_DIR_UNAGG)/%.o: $(SRC_DIR)/%.cpp $(DEPS)
	$(UPCXX) $(CPPFLAGS) $(CXXFLAGS) $(EXTRA_FLAGS) -c $< -o $@

$(OBJ_DIR_AGG)/%.o: $(SRC_DIR)/%.cpp $(DEPS)
	$(UPCXX) $(CPPFLAGS) $(CXXFLAGS) $(EXTRA_FLAGS) -c $< -o $@

$(OBJ_DIR_COMMON_GUP)/%.o: $(SRC_DIR_COMMON_GUP)/%.cpp $(DEPS)
	$(UPCXX) $(CPPFLAGS) $(CXXFLAGS) $(EXTRA_FLAGS) -c $< -o $@

$(OBJ_DIR_COMMON)/%.o: $(SRC_DIR_COMMON)/%.cpp $(DEPS)
	$(UPCXX) $(CPPFLAGS) $(CXXFLAGS) $(EXTRA_FLAGS) -c $< -o $@


PROCS ?= 4
NODES ?= 
ARGS ?= -l 24
LINE = =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-

run: $(TARGETS)
	@for file in $(TARGETS) ; do \
	  if test -x $$file ; then \
	    echo $(LINE) ; \
	    ( set -x ; \
	      $(UPCXXRUN) -np $(PROCS) $(NODES) $$file $(ARGS) ; \
	    ) ; \
	    echo $(LINE) ; \
	  fi ; \
	 done

clean:
	$(RM) -rf $(TARGETS) *.d *.dSYM core*
	$(RM) -rf $(OBJ_DIR)
force:

.PHONY: all clean force run directories

directories:
	$(MKDIR) $(OBJ_DIR)

RM        := rm -f
MKDIR     := mkdir -p
RMDIR     := rmdir
IGNORE_ERR    := >& /dev/null
