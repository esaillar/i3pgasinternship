#ifndef _gup_utils_hpp
#define _gup_utils_hpp

#include <string>
#include "colors.hpp"
#include "types.hpp"

using namespace std;

// Types used by program should be 64 bits
// https://stackoverflow.com/questions/2268749/defining-global-constant-in-c#2268826

// Random number generator
const uint64 POLY = 0x0000000000000007UL;
const uint64 PERIOD = 1317624576693539401L;

void Block(int mype, int npes, const int64& totalsize,
           int64& start, int64& stop, int64& size);

uint64 startr(int64 n);

double MUPS(double t, int64 nupdate);

extern int Mod;

bool isAll2All();
bool isNewRule();
bool isSrcComplete();
bool isFlushMode();
bool isFlushLocalMode();

#endif
