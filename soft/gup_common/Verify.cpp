#include <assert.h>
#include <cmath>
#include <chrono>
#include "gup_utils.hpp"
#include "gup.hpp"
#include "Comm.hpp"
#include "OrderedStream.hpp"
//#define NEW 1
//#define DEBUG 0

using namespace std;
const int CheckRank = 4;
int mTabSize;


//  Measure errors. These occur when there are data races
//  in the implementation.
//  However there are several problems with our measurement technique
//  1) The validation code measures incorrect final table values, which 
//     approximates updates lost due to data races. Thes losses cannot be
//     reliably determined after-the-fact; the collision of two update
//     writes to the same table entry may result in a meaningless value.
//     With high probability, every lost update implies that the targeted
//     table entry will have an incorrect final value, but it's very
//     possible for two or more lost updates to conflate into the same
//     incorrect final table entry, resulting in an under-reporting of
//     the true number of lost updates.
//
//  2) The number of updates is (by default) four times the number of table
//     entries.  Assuming no conflation of failed updates, the relative error
//     will be over-reported by up to a factor of four.
//
//  3) The specified 1% error threshold was designed to tolerate random
//     collisions in the batches of max size 1024 updates permitted by
//     the benchmark's lookahead restrictions, assuming a uniform random
//     distribution of updates in a table size comparable to half of the
//     system's physical memory (another benchmark requirement).
//     However, running at smaller table sizes (as this implementation allows)
//     will magnify the collisions and hence the reported error for all
//     batched algorithms.

// Verification.
// We are only interested in knowing whether or not there were any errors.
// GUPS' motivation for verification is not important for our purposes,
// since there can be no race condition.

int64 Verify(const int64 locTabSize, const int64 nupdate, uint64 *table, int64 i0, int64 i1, const int64 tabsize, const bool errByRank){

    // We simply check that table matches the initial conditions
    // This version of Verify relies on the reversibility of the updates
    // We simply perform the GUPS updates an additional time
    // if the number of reps is a random number (take care of in update_table)

    ostringstream sl;

    intrank_t myrank = comm->myRank();
    const uint64 Pm1 = comm->nRanks()-1;
    const int iTabSz = ilog2(locTabSize);

    comm->Barrier();

    int64 nerrors = 0;
    for (int64 i = 0; i < locTabSize; i++){
        nerrors += (table[i] != (uint64) (i+i0));
#ifdef DEBUG
        if ( !(i % 1000) && (table[i] != (uint64) (i+i0)) && (myrank == 2)){
             sl << "[" << myrank<< "] (" << i << ")" << table[i] << "!=" << i+i0  << endl;
        }
#endif
    }

    // 2. Accumulate and report
    int64 totalErrors = comm->gsum_int64(nerrors, true);

    comm->Barrier();

//    int ners = nerrors;
//    int64 totalErrors = comm->gsum_int(ners, true);
//    if (nerrors == 0)
//        sl << constants::YELLOW << "No Errors!\n" << "\x1B[0m";
//    else
    if ((totalErrors > 0) && (nerrors > 0) && errByRank){
        sl << constants::RED << nerrors << " Errors! [" << myrank << "]" << endl << constants::BACK;
    }
    sordst.OutputStream(sl);

    return totalErrors;
}

int64 Verify_NS(const int64 locTabSize, const int64 nupdate, uint64 *table, int64 i0, int64 i1, const int64 tabsize, const bool errByRank){

    // ***  Not scalable.  We have to go through the entire
    //      set of random numbers, picking off the ones we own
    //      If we have each rank go through its part of the index domain
    //      we have to combine all the results at the end,
    //      and o(lg(P)*N) operation
    //      We need an oracle that will generate only the random
    //      numbers that we own :-)

    ostringstream sl;

    // 1. Verify local results
    // The induction variable is never used except to count iterations
    // So we use a doubly nested loop so all loop count arithmetic
    // is done as an int rather than int64

    // Obtain M, a factor of nupdate that's close to sqrt(nupdate)
    int64 m = q_factor(nupdate);
    const int N = (int) (nupdate /  m);
    const int M = (int) m;
    assert(m*(int64) N  == nupdate);

    intrank_t myrank = comm->myRank();
    const uint64 Pm1 = comm->nRanks()-1;
    const int iTabSz = ilog2(locTabSize);

    // Split up the table
    const uint64 tabszm1 = tabsize-1;
    const int64 ltabszm1 = locTabSize-1;

    // Begin update timing here
#ifdef DEBUG
        if (myrank == CheckRank)
            cout << "Verify0" << endl;
#endif
    comm->Barrier();
    auto T0 = chrono::steady_clock::now();

    uint64 r = 0x1;
    for (int i=0; i<M; i++) {
       for (int j=0; j<N; j++) {
#ifdef DEBUG
          uint64 was = r;
#endif
          r = (r << 1) ^ (((int64) r < 0) ? POLY : 0);
          const int targ = (r >> iTabSz) & Pm1;   // Determine target
#ifdef DEBUG
          if (myrank == CheckRank)
            printf("r: (%llx) --> (%llx), targ= %d\n",was,r, targ);
#endif
          // Subtract global offset
          if ( targ == myrank){
              const int loffs = r & ltabszm1;
              const uint64 twas = table[loffs];
              table[loffs] ^= r;
#ifdef DEBUG
              if (myrank == CheckRank)
                   printf("U [%d] += ({@%llx}(%llx) --> (%llx)\n",loffs,r,twas,table[loffs]);
#endif
          }
        } // end j
    } // end i

	
    int64 nerrors = 0;
    for (int64 i = 0; i < locTabSize; i++){
        nerrors += (table[i] != (uint64) (i+i0));
#ifdef DEBUG
        if (table[i] != (uint64) (i+i0)){
             cout << "❖[" << myrank<< "] (" << i << ")" << table[i] << "!=" << i+i0  << endl;
        }
#endif
    }

    // 2. Accumulate and report
    int64 totalErrors = comm->gsum_int64(nerrors, true);

    comm->Barrier();

    // End update timed section
    auto T1 = chrono::steady_clock::now();
    chrono::duration<double> t_walltime = T1-T0;
    double Tw = t_walltime.count();
    double twmax = comm->gmax_double(Tw, false);
    if (!myrank)
        cout <<  "\n*****> Verification time: " << twmax << " (SLOW verification)" << endl;
//    sordst.OutputStream(sl);
//    int ners = nerrors;
//    int64 totalErrors = comm->gsum_int(ners, true);
//    if (nerrors == 0)
//        sl << constants::YELLOW << "No Errors!\n" << "\x1B[0m";
//    else
    if ((totalErrors > 0) && (nerrors > 0) && errByRank){
        sl << constants::RED << nerrors << " Errors! [" << myrank << "]" << constants::BACK;
    }
    sordst.OutputStream(sl);

    return totalErrors;
}
