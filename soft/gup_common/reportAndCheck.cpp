#include <iostream>
#include <math.h>
#include <map>
#include <assert.h>

#include "gup.hpp"
#include "gup_utils.hpp"
#include "utils.hpp"
#include "Comm.hpp"
#include "OrderedStream.hpp"

using namespace std;

// For printing out the environment
extern char **environ;

void printCoreAffinity(int);

int reportAndCheck(const int Vlen, const int Alen, const int nreps,
                   int& kfactor, const int l_tabsize, const int m_update,
                   const bool printAffinity, int Mode){

    intrank_t myrank = comm->myRank();
    intrank_t nranks = comm->nRanks();
    const bool All2All = Mode & 1;

    const int64 tabsize = ((int64) 1) << (int64) l_tabsize;
    const int64 nupdate = ((int64) m_update) * tabsize;

    ostringstream sl;

    if (isFlushMode() && isFlushLocalMode()){
        FATAL_ERR("You cannot enable both Flush Mode and Flush Local Mode.");
    }
//    sl << Mode<< Mod << endl;
    sordst.OutputStream(sl);
    if (isNewRule() && isAll2All()){
        FATAL_ERR("You cannot use the new storage allocation mode with the All2All variant.");
    }
    // Check for errors
    if ( (nranks <= 0) || (!((nranks & (nranks-1)) == 0)) || (tabsize % nranks)){
        FATAL_ERR("Number of ranks (" << nranks << ") must be a power of 2 that divides the global table size (" << tabsize << ") evenly.");
    }


    if (!((m_update & (m_update-1)) == 0))
        FATAL_ERR("Aggregation factor [" << m_update << "] must be a power of 2.");

    if (Alen == 0) {
        if (!All2All){
            if (alg_name.compare("MPI-AGG") == 0)
                FATAL_ERR("You need a non-zero aggregation factor.");

            sl << "No Aggregation" << endl;
        }
    }

    else if (Alen > 0){
        if (!((Alen & (Alen-1)) == 0))
            FATAL_ERR("Aggregation factor [" << Alen << "] must be a power of 2.");
        if (Vlen < Alen){
            FATAL_ERR("Aggregation factor [" << Alen << "] must be smaller than the Batch size [" << Vlen << "]");
        }
    }
    else{
        FATAL_ERR("The Aggregation factor must be non-negative.");
    }
    sordst.OutputStream(sl);

    int64 start, stop, size;
    Block(myrank, nranks, nupdate, start, stop, size);

    if (!((Vlen & (Vlen-1)) == 0))
        FATAL_ERR("Launch window size [" << Vlen << "] must be a power of 2.");

    if (Vlen > size){
        FATAL_ERR("Batch size [" << Vlen << "] cannot exceed the number of updates assigned to a rank [" << size << "]");
    }

    if (Vlen < nranks){
        FATAL_ERR("Batch size [" << Vlen << "] cannot be smaller than the number of ranks [" << nranks << "]");
    }
    
#ifdef DEBUG
   // For printing out the environment
   // https://stackoverflow.com/questions/16765545/how-to-list-all-environment-variables-in-a-c-c-app
    comm->Barrier();
    if (!myrank){
        cout << "Environment " << endl;

        for(char **current = environ; *current; current++) {
                    char *found = strstr(*current,"32");
                    char *found2 = strstr(*current,"COLORS");
                    auto nomatch = (strstr(*current,"PMIX") == NULL) && (strstr(*current,"tcp") == NULL);
                    if ((found != NULL) && (found2 == NULL) && nomatch)
                        cout << "(" << myrank << "): " <<  *current << endl;
            }
        cout << "# ranks/node [" << myrank << "]: " <<   getNumRanksPerNode() << endl;
    }
    comm->Barrier();
#endif

    // Flush the stream
    sordst.OutputStream(sl);
    comm->Barrier();

    if (!myrank){
        cout << endl << ">---> Run starts at ";
        timestamp(true);
    }

    comm->Barrier();

    if (printAffinity){
        printCoreAffinity(myrank);
    }

        // Everything that's "cout" was "sl"
    sl << "Vlen = " << Vlen << endl;
//        cout << "Vlen = " << Vlen << endl;
   
        // Adjust as needed
    const int SIZE_THRESH = 256;
    if ((tabsize/Vlen) > SIZE_THRESH){
        sl << " *** The ratio of the table size to Vlen > " << SIZE_THRESH << ".\n     You may overflow the aggregation buffer. Try increasing Vlen." << endl << endl;
    }

    if (All2All)
        sl << "All to all method; aggregation factor is infinite" << endl;
    else
        sl << "Alen (Aggregation Factor) = " << Alen << endl;

    if (nreps > 0)
        sl << "# reps = " << nreps << endl;

    if ((Alen == 0) && (!All2All)){
        if (kfactor > 0){
            sl << "Ignoring the k factor since we aren't aggregating." << endl;
            kfactor = 0;
        }
    }
    else{
        if (kfactor == 0)
            kfactor = 4;
    }
    sordst.OutputStream(sl);
    comm->Barrier();

    int n_nodes = nNodes();
    int n_ranksPerNode = getNumRanksPerNode();
    if (isNewRule())
        sl << "k factor (imbalance) = " << kfactor << " [NEW Rule: x"  <<  Alen << "]" << endl;
    else
        sl << "k factor (imbalance) = " << kfactor << " [Old Rule: x" << kfactor << "*expect (" <<  Vlen << "/" << nranks << "=" << Vlen/nranks << ") = kfactor*Vlen/nranks (" << kfactor*Vlen/nranks << ")]" << endl;

    sl << "# ranks = " << nranks << endl;
    sl << "# nodes = " << n_nodes << endl;

    sl << "# ranks per node = " <<  n_ranksPerNode << endl << endl;

    sl << "# of updates: " << nupdate << endl;
    sl << "Mode: " << Mode << " ";
    if (isSrcComplete())
        sl << "[Source Completion] ";
    if (isNewRule())
        sl << "[New storage allocation Mode] ";
    if (isFlushMode())
        sl << "[Using Flush] ";
    if (isFlushLocalMode())
        sl << "[Using Flush Local] ";

    if (isAll2All())
        sl << "[All to All] ";
    else
        sl << "[Put]";

    sl  << endl;

    sordst.OutputStream(sl);
    comm->Barrier();

#ifdef LATER
    if (Mode == 0)
        sl << "   -> Source Completion" << endl;
    else
        sl << "   -> NO Source Completion" << endl;
#endif


#ifdef NG
    const int ncores = getNumCores();
    if (ncores == -1)
        sl << constants::RED << "# cores/node = " << ncores << "\n" << constants::BACK << endl;
    else
        sl << "# cores/node = " << ncores << endl;
#endif


    sordst.OutputStream(sl);
    comm->Barrier();
    reportMemory("Start");

    return(0);
}
