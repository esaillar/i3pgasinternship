#ifndef _gup_hpp
#define _gup_hpp
//  This is derived from upcxx-extras gups code: it has been ported to MPI-RMA
//  Copyright 2018, The Regents of the University of California
//  Terms of use are as specified in LICENSE.txt
//  Scott B. Baden and Dan Bonachea, upcxx@googlegroups.com
//
//   UPC++ version of gups based on the upc version [001222 mhm]
//   See http://icl.cs.utk.edu/projectsfiles/hpcc/RandomAccess/
//

#include <inttypes.h>
#include <string>
#include "types.hpp"
#include "colors.hpp"

using namespace std;

using stats_tuple = pair<int64x2,intx2>;

extern string alg_name; // algorithm name


// Mode bit usage

void reportResults(double walltime, int64 nupdate, int ltabsize, 
                   int Vlen, int Alen, int freq,
                   stats_tuple SlotUsage, bool doVerification, int Mode);

void reportSummary(int nreps, int64 nupdate, int vlen, int n_agg, bool
        hasSlots,bool doVerification, int64 totErrs, int Mode);

string getJobString();

int64 update_table(const int nreps,
                  const int Vlen, const int n_agg, const int ltabsize, const int64 tabsize,
                  const int64 nupdate, const int freq,
                  const bool errByRank, const bool doVerification);

int64 update_table_base(const int nreps,
                  const int Vlen, const int n_agg, const int ltabsize, const int64 tabsize,
                  const int64 nupdate, const int freq,
                  const bool errByRank, const bool doVerification);

int64 update_table_aggreg(const int nreps,
                  const int Vlen, const int n_agg, const int ltabsize, const int64 tabsize,
                  const int64 nupdate, const int freq,
                  const bool errByRank, const bool doVerification);

int64 update_table_put(const int nreps,
                  const int Vlen, const int n_agg, const int ltabsize, const int64 tabsize,
                  const int64 nupdate, const int freq,
                  const bool errByRank, const bool doVerification);

int64 update_table_alltoallv(const int nreps,
                  const int Vlen, const int n_agg, const int ltabsize, const int64 tabsize,
                  const int64 nupdate, const int freq,
                  const bool errByRank, const bool doVerification);

int64 update_table_local(const int nreps,
                  const int Vlen, const int n_agg, const int ltabsize, const int64 tabsize,
                  const int64 nupdate, const int freq,
                  const bool errByRank, const bool doVerification);


void reportResults(double walltime, int64 nupdate, int ltabsize,
                   int Vlen, int Alen, int freq,
                   bool doVerification, int Mode);

int64 Verify(const int64 locTabSize,const int64 nupdate, uint64 *table, int64 i0, int64 i1, const int64 tabsize, const bool errByrank);
#endif
