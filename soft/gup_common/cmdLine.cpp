//
//  This is derived from upcxx-extras gups code.
//  Terms of use are as specified in UPC++_LICENSE.txt
//

#include <getopt.h>
#include <sstream>
#include <assert.h>

using namespace std;

void cmdLine(int argc, char *argv[], int& l_tabsize, int& m_update, int& nreps,
        int& Vlen, int& Alen, int& kfactor,
        bool& printAff, bool& errorByRank, bool& doVerification,
        int& freq, int& mode){

   static struct option long_options[] = {
      {"l_tabsize", required_argument, NULL, 'l'},// Log of table size
      {"m_update", required_argument, NULL, 'm'}, // Multiplier
                                                  // # updates = M*table size
      {"Affinity", no_argument, NULL, 'y'},       // Print processor affinity
      {"ErrbyRank", no_argument, NULL, 'e'},      // Print error by rank
      {"Verify", no_argument, NULL, 'c'},         // Verify result
      {"Freq", required_argument, NULL, 'f'},    // # progress frequency
      {"nreps", required_argument, NULL, 'r'},    // # Repetitions
      {"kfactor", required_argument, NULL, 'k'},  // Load imbalance factor
      {"Vlen", required_argument, NULL, 'v'},     // Vector length
      {"Alen", required_argument, NULL, 'a'},     // Aggregation factor
      {"Mode", required_argument, NULL, 'M'},     // Mode (various ways to
                                                  // configure, open-ended
   };
   // Parse the command line arguments -- *if something is not kosher, die
   for(int ac=1;ac<argc;ac++) {
       int c;
       ostringstream sl1;
       while ((c=getopt_long(argc,argv,"l:m:r:k:v:a:A:f:h:M:yec?",long_options,NULL)) != -1)
       {
        switch (c) {

            case 'l':
                l_tabsize = atoi(optarg);
                break;

            case 'm':
                m_update = atoi(optarg);
                break;

            case 'r':
                nreps = atoi(optarg);
                break;

            case 'k':
                kfactor = atoi(optarg);
                break;

            case 'v':
                Vlen = atoi(optarg);
                break;

            case 'a':
                Alen = atoi(optarg);
                break;

            case 'f':
                freq = atoi(optarg);
                break;

            case 'M':
                mode = atoi(optarg);
                break;

            case 'y':   // Print processor affinity
                printAff = true;
                break;

            case 'e':   // Print breakdown of error by rank (if > 0)
                errorByRank = true;
                break;

            case 'c':   // Do verification
                doVerification = true;
                break;

	    // Error
            case '?':
//	       if (!myrank)
//		   std::cerr << "Error in command line argument: " << optarg << std::endl;
               exit(-1);
	       break;

            default:  // You can't get here !!
	       break;
          }
      }
  }
}

