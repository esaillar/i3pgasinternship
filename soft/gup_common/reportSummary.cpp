#include <iostream>
#include <string.h>
#include "gup_utils.hpp"
#include "utils.hpp"
#include "Comm.hpp"
#include "gup.hpp"


using namespace std;

// Used to report summary statistic
extern double *wallTimes;

// These will be useful when aggregation is enabled
extern int64 *maxSlots;
extern int   *maxSlotsHW;

extern bool *pfs;

// Need to fix this so it doesn't report slots w/o aggregation
void reportSummary(int nreps, int64 nupdate, int vlen, int n_agg,
                   bool hasSlots,bool doVerification, int64 totErrs, int Mode){
    double minTime=1e9, maxTime=-1, sumTimes = 0;

    comm->Barrier();
    intrank_t myrank = comm->myRank();
    intrank_t nranks = comm->nRanks();

    if (!myrank){
        // Ignore the first time, it's always slow
        for (int i=1; i<nreps; i++){
            double t = wallTimes[i];
            if (t<minTime){
                minTime = t;
            }
            if (t>maxTime){
                maxTime = t;
            }
            sumTimes += t;
        }
        int64 Max_Slots = -1;
        int Max_SlotsHW = -1;
        if (hasSlots){
            if (n_agg > 0){
                for (int i=1; i<nreps; i++){
//                    cout << "m["<< i << "]: " << maxSlots[i] << endl;
                    if (maxSlots[i] > Max_Slots){
                        Max_Slots = maxSlots[i];
                    }
                    if (maxSlotsHW[i] > Max_SlotsHW){
                        Max_SlotsHW = maxSlotsHW[i];
                    }
                }
            } else{
                cout << " ******* Doesn't have Slots " << endl;
            }
        }
        double meanTime = sumTimes / (double (nreps-1));
        char p_f[2]= "P";
        char SummH[] = "Summary";
        if (!doVerification)
            strcpy(p_f, "?");
        else if (totErrs > 0)
            strcpy(p_f, "F");
        string jstring = getJobString ();
        const bool All2All = Mode & 1;

        char N_AGG[6] = "  INF";
        if  ((!All2All) && (alg_name.compare("MPI-PUT") != 0))
            sprintf(N_AGG,"%5d",n_agg);
        if ((n_agg > 0) || All2All || ((alg_name.compare("MPI-PUT") == 0))){
            // *** 4 is hardwired, should use the defined constant
            const int L = ilog2(nupdate / 4);
            printf("\n\t\tMax       Min       Avg      L      P       V     A   P/F   Push   HighW  Reps         Alg   Mode\n");
            printf("%s*   %9.2f %9.2f %9.2f    %2d   %5d", SummH, MUPS(minTime,nupdate), MUPS(maxTime,nupdate), MUPS(meanTime,nupdate), L, nranks);
            if (vlen < 1024)
                printf("   %4d ",vlen);
            else {
                int vlenk = vlen / 1024;
                printf(" %5dK ", vlenk);
            }
            if (hasSlots){
                printf("%5s    %1s %7lld  %6d   %3d", N_AGG, p_f, (long long int) Max_Slots, Max_SlotsHW, nreps);
            }
            else{
                printf("%5s    %1s     --     --     %3d", N_AGG, p_f, nreps);
            }
            printf("    %s   %2d", alg_name.c_str(), Mode);
            printf("  ");
            if (jstring.compare(" -1") != 0){
                timestamp(false);
                printf("  [%s]\n",jstring.c_str());

            }
            else{
                timestamp(true);
            }
        }
        else{
            printf("\n\t       Max       Min      Avg       V        P/F Reps");
            printf(" Alg        Mode\n");
            printf("%s* %9.2f %9.2f %9.2f  %4d        %1s %3d",
                    SummH, MUPS(minTime,nupdate), MUPS(maxTime,nupdate),
                    MUPS(meanTime,nupdate), vlen, p_f, nreps);
            printf("    %s   %2d    ", alg_name.c_str(), Mode);
            printf("  ");
            if (jstring.compare(" -1") != 0){
                timestamp(false);
                printf("  [%s]\n",jstring.c_str());

            }
            else{
                timestamp(true);
            }
        }
        cout << "* Summary performance ignores first iteration" << endl;
    }
    comm->Barrier();
}
