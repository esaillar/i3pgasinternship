#include <iostream>
#include <string.h>
#include "utils.hpp"
#include "Comm.hpp"
#include "gup.hpp"

using namespace std;

// Used to report summary stats
extern double *wallTimes;
extern int64 *maxSlots;
extern int *maxSlotsHW;
extern int nentries;

static bool before = false;

string printHeader(int n_agg, int Mode){

    char JOB[32]= "";
    bool isJob = false;
    string jstring;
    if (!before){
        jstring = getJobString ();
        if (jstring.compare(" -1") != 0){
            isJob = true;
            strcpy(JOB, "         Job");
            jstring = ", " + jstring;
        }
        else{
            jstring = "";
        }

        const bool All2All = Mode & 1;
        if ((n_agg > 0) || All2All){
            printf("@Lsz,   Alg,    M,       P,      Mups,      tWall,   vlen,    nAgg,  Push,  HighW,       TStamp, %s\n",JOB);
        } else{
            printf("@Lsz,   Alg,    M,     P,      Mups,");
            printf("      tWall,     vlen,  TStamp,     %s\n",JOB);
        }
    }
    before = true;
    return jstring;

}

void reportResults(double walltime, int64 nupdate, int ltabsize,
                   int vlen, int n_agg, int freq, stats_tuple slotsTallies,
                   bool doVerification, int Mode){
    intrank_t myrank = comm->myRank();
    intrank_t nranks = comm->nRanks();
    int64 mxSlots = slotsTallies.first.first;
    int64 mnSlots = slotsTallies.first.second;
    int maxHighW = slotsTallies.second.first;
    int minHighW = slotsTallies.second.second;

    comm->Barrier();

    if ( !myrank ) {
        // Collect summary results

        wallTimes[nentries] = walltime;
        if (mxSlots > 0)
            maxSlots[nentries] =  mxSlots;
        else
            maxSlots[nentries] =  0;

        if (maxHighW > 0)
            maxSlotsHW[nentries] = maxHighW;
        else
            maxSlotsHW[nentries] = 0;

        if ((mxSlots >= 0) || (mnSlots >= 0)){
            cout << endl << "Max, Min total pushes:   " << mxSlots << ", " << mnSlots << endl; 
            cout << "Max, Min slot highwater: " << maxHighW << ", " << minHighW << endl << endl;
        }

        // Print timing results
        double mups = (double) nupdate * 1e-6 / walltime;
        cout << fixed;
#ifdef VERBOSE
        cout << left << "Update time =";
        cout << right << setw(8) << setprecision(4) << walltime;
        cout << left << " s." << endl;
        cout << left << " s.   |   Rate = ";
        cout << right << setw(8) << setprecision(4) << mups;
        cout << left << " Mup/s" << endl;
#endif

        string jstring = printHeader(n_agg,Mode);
        const bool All2All = Mode & 1;
        if ((n_agg > 0) || All2All) {
            if (vlen > 1023){
                int vlenk = vlen / 1024;
                printf("@ %d, %s, %2d, %5d, %9.2f,  %9.2f, %5dK,", ltabsize, alg_name.c_str(), Mode, (int) nranks, mups, walltime, vlenk);
                if (All2All)
                    printf("     INF, ");
                else
                    printf("%7d",n_agg);
                printf(" %7lld,  %7d,   ", (long long int) mxSlots, maxHighW);
//                printf("@ %d, %s, %2d, %5d, %9.2f,  %9.2f, %5dK, %7d, %7lld,  %7d,   ", ltabsize, alg_name.c_str(), Mode, (int) nranks, mups, walltime, vlenk, All2All ? -1 : n_agg, (long long int) mxSlots, maxHighW);
                timestamp(false);
                cout << jstring.c_str();
            }
            else{
                printf("@ %d, %s, %2d, %5d, %9.2f,  %9.2f, %5d, %7d, %7lld,  %7d,   ",
                       ltabsize, alg_name.c_str(), Mode, (int) nranks, mups,
                        walltime, vlen, All2All ? -1 : n_agg, (long long int) mxSlots,maxHighW);
                timestamp(false);
                cout << ", ";
                cout << jstring.c_str();
            }
            cout << endl;
        } else{
            printf("@ %d, %s, %2d, %5d, %9.2f,  %9.2f,  %7d,   ", 
                ltabsize, alg_name.c_str(), Mode, (int) nranks, mups, walltime, vlen);
                timestamp(false);
                cout << jstring.c_str() << endl;
        }
    }
//    comm->Barrier();
    return;
}
