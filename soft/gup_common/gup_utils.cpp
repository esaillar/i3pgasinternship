#include <assert.h>
#include <cmath>
#include <chrono>
#include "types.hpp"
#include "gup_utils.hpp"

//
//  A port of the upcxx-extras GUPS code
//  Terms of use are as specified in UPC++_LICENSE.txt
//

using namespace std;

//
//  Block-decompose size: loop iters or space amount)
//  This is a general purpose routine that works
//  when npes doesn't divide size
//  But GUPS requires that npes divides the total table size.
//  Thus, size = totalsize / npes
//

void Block(int mype, int npes, const int64& totalsize,
           int64& start, int64& stop, int64& size)
{
    int64 div;
    int64 rem;

    div = totalsize / npes;
    rem = totalsize % npes;

    if (mype < rem)
    {
        start = mype * (div + 1);
        stop   = start + div;
        size  = div + 1;
    }
    else
    {
        start = mype * div + rem;
        stop  = start + div - 1;
        size  = div;
    }
}

//
//  Start the random number generator at Nth step
//

uint64 startr(int64 n)
{
    int i, j;
    uint64 m2[64];

    while (n < 0) n += PERIOD;
    while (n > PERIOD) n -= PERIOD;
    if (n == 0) return 0x1;

    uint64 temp = 0x1;
    for (i=0; i<64; i++)
    {
	m2[i] = temp;
	temp = (temp << 1) ^ ((int64) temp < 0 ? POLY : 0);
	temp = (temp << 1) ^ ((int64) temp < 0 ? POLY : 0);
    }
    
    for (i=62; i>=0; i--)
	if ((n >> i) & 1)
	    break;

    uint64 ran = 0x2;
    while (i > 0)
    {
	temp = 0;
	for (j=0; j<64; j++)
	    if ((ran >> j) & 1)
		temp ^= m2[j];
	ran = temp;
	i -= 1;
	if ((n >> i) & 1)
	    ran = (ran << 1) ^ ((int64) ran < 0 ? POLY : 0);
    }
  
    return ran;
}    

double MUPS(double t, int64 nupdate){
    double mups = (double) nupdate * 1e-6 / t;
    return(mups);
}


bool isAll2All(){
    return (Mod & 1);
}

bool isNewRule(){
    return ((Mod  >> 1) & 1);
}

bool isSrcComplete(){
    return ((Mod  >> 2) & 1);
}

bool isFlushMode(){
    return ((Mod  >> 3) & 1);
}

bool isFlushLocalMode(){
    return ((Mod  >> 4) & 1);
}
